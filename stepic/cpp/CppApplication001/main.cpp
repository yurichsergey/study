#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
	int strstr(const char *, const char *);
	// char text[] = "string 1";
	// char pattern[] = "n";
	// int pos = strstr(text, pattern);
	// cout << pos << endl;

	// cout << strstr("iuyuitigi", "gf") << endl;
	// cout << strstr("", "") << endl;
	// cout << strstr("68yh", "") << endl;
	// cout << strstr("", "hy78n") << endl;
//	cout << strstr("asda", "as") << endl<< endl;
//	cout << strstr("sasda", "as") << endl<< endl;
//	cout << strstr("string 1", "n") << endl;

//    char* str = new char[20];
//    str[0] = 'e';   str[1] = 'w';   str[13] = 's';   str[19] = '\0';
//    unsigned sizeStr = 1;	 // Учтем последний символ '\0'
//	for (char * s = str; *s != '\0' ; s++ ) {
//		sizeStr++;
//	}
//    char *resize(const char *, unsigned , unsigned );
//    cout << resize(str, sizeStr, 40) << endl;

    char * getline();
    cout << getline() << endl;
	return 0;
}


/** Реализуйте функцию getline, которая считывает поток ввода посимвольно, пока не достигнет конца потока или не
 *      встретит символ переноса строки ('\n'), и возвращает C-style строку с прочитанными символами.
 * Обратите внимание, что так как размер ввода заранее неизвестен, то вам нужно будет перевыделять память в процессе
 *      чтения, если в потоке ввода оказалось больше символов, чем вы ожидали.
 * Память, возвращенная из функции будет освобождена оператором delete[]. Символ переноса строки ('\n') добавлять в
 *      строку не нужно, но не забудьте, что в конце C-style строки должен быть завершающий нулевой символ.
 */
char *getline()
{
    char symbol = '\0';
    unsigned sizeBlock = 10;

    unsigned sizeStr = sizeBlock;
    char * str = new char[sizeStr];
//    char * newStr = 0;

    unsigned i = -1;
	while (cin.get(symbol) && symbol != '\0' && symbol != '\n') {
//        i++;
		if (++i >= sizeStr - 2) {     // -1 - нумерация с 0,  -1 - для символа '\0'
            char *resize(const char *, unsigned , unsigned );
//			newStr = resize(str, sizeStr, sizeStr + sizeBlock);
//            str = newStr;
			str = resize(str, sizeStr, sizeStr + sizeBlock);
            sizeStr += sizeBlock;
		}
		str[i] = symbol;
	}
    str[++i] = '\0';
	return str;
}

char *resize(const char *str, unsigned size, unsigned new_size)
{
	unsigned copySize = size > new_size ? new_size : size;
    char * newStr = new char [new_size];
    for(unsigned i = 0; i < copySize; i++) {
        newStr[i] = str[i];
    }
    delete[] str;
    return newStr;
}

int strstr(const char *text, const char *pattern)
{
	const char* sText = text;
	const char* sPattern = pattern;
	int pos = -1;

	int sizePattern = 1;	 // Учтем последний символ '\0'
	for (; *sPattern != '\0' ; sPattern++ ) {
		sizePattern++;
	}
	sPattern = pattern;

//	cout << "spattern " << sPattern << endl;

	const char* currentSymbolPattern = pattern;
	if (*currentSymbolPattern == '\0') {
		pos = 0;
		return pos;
	}
	const char* firstMatchText = text;
    int i = -1;
	for (; *sText != '\0'; sText++) {
        i++;
		if (*sText == *currentSymbolPattern) {
			if (pos == -1) {
				pos = i;
				firstMatchText = sText;
			}
			if (*(++currentSymbolPattern) == '\0') {
				break;
			}
		} else {
			currentSymbolPattern = pattern;
			pos = -1;
		}
	}
//    cout << " pos = " << pos << "\t i = " << i << " \t sizePattern = " << sizePattern << " \t";
	if ((i + 1) - pos < sizePattern - 1) {
		pos = -1;
	}
    return pos;
}