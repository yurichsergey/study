'''
Реализуйте программу, которая будет эмулировать работу с пространствами имен. Необходимо
 реализовать поддержку создания пространств имен и добавление в них переменных.

В данной задаче у каждого пространства имен есть уникальный текстовый идентификатор – его имя.
'''


import json


' ========================================== '


def parse_input_data_as_array(arr):
    result = []
    for line in arr:
        match = line.split(' ')
        result.append(match)
    return result


def print_arrays_from_string(arr):
    # print(len(arr))
    for line in arr:
        print(line)
    return True


def read_from_console():
    result = []
    n_lines = int(input())
    for i in range(n_lines):
        result.append(input())
    return result


def read_from_file(file_name):
    result = []
    with open(file_name, 'r') as inf:
        is_first_str = True
        for line in inf:
            if is_first_str:
                is_first_str = False
                continue
            result.append(line.replace("\n", ""))
    return result


' ========================================== '

def find_in_array(namespaces_dict, namespace, value):
    result = 'None'
    if namespace in namespaces_dict:
        parent = namespaces_dict[namespace]['parent']
        if value in namespaces_dict[namespace]['vars']:
            result = namespace
        elif 'None' != parent:
            result = find_in_array(namespaces_dict, parent, value)
    return result


def create_namespaces(arr_command):
    result_print = []
    namespaces_dict = {
        'global': {
            'parent': 'None',
            'vars': []
        }
    }
    for line in arr_command:
        if len(line) == 3:
            operation = line[0]
            namespace = line[1]
            value = line[2]
            if 'create' == operation:
                namespaces_dict[namespace] = {
                    'parent': value,
                    'vars': []
                }
            elif 'add' == operation:
                namespaces_dict[namespace]['vars'].append(value)
            elif 'get' == operation:
                result_print.append(find_in_array(namespaces_dict, namespace, value))

    # print(json.dumps(namespaces_dict, indent=4))
    return result_print



input_data = read_from_console()
# input_data = read_from_file('./data/namespaces_modulate_IN.txt')
# print_arrays_from_string(input_data)
# print("\n\n")

parse_data = parse_input_data_as_array(input_data)
# print(json.dumps(parse_data, indent=4))
# print('')

result_print = create_namespaces(parse_data)
# print(json.dumps(result_print, indent=4))
# print('')

for row in result_print:
    print(row)


'''
Реализуйте программу, которая будет эмулировать работу с пространствами имен. Необходимо
 реализовать поддержку создания пространств имен и добавление в них переменных.

В данной задаче у каждого пространства имен есть уникальный текстовый идентификатор – его имя.

Вашей программе на вход подаются следующие запросы:

create <namespace> <parent> –  создать новое пространство имен с именем <namespace> внутри 
пространства <parent>
add <namespace> <var> – добавить в пространство <namespace> переменную <var>
get <namespace> <var> – получить имя пространства, из которого будет взята переменная <var>
 при запросе из пространства <namespace>, или None, если такого пространства не существует
Рассмотрим набор запросов

add global a
create foo global
add foo b
create bar foo
add bar a
Структура пространств имен описанная данными запросами будет эквивалентна структуре 
пространств имен, созданной при выполнении данного кода

a = 0
def foo():
  b = 1
  def bar():
    a = 2
В основном теле программы мы объявляем переменную a, тем самым добавляя ее в пространство 
global. Далее мы объявляем функцию foo, что влечет за собой создание локального для нее 
пространства имен внутри пространства global. В нашем случае, это описывается командой 
create foo global. Далее мы объявляем внутри функции foo функцию bar, тем самым создавая 
пространство bar внутри пространства foo, и добавляем в bar переменную a.

Добавим запросы get к нашим запросам

get foo a
get foo c
get bar a
get bar b
Представим как это могло бы выглядеть в коде

a = 0
def foo():
  b = 1
  get(a)
  get(c)
  def bar():
    a = 2
    get(a)
    get(b)
 
Результатом запроса get будет имя пространства, из которого будет взята нужная переменная.
Например, результатом запроса get foo a будет global, потому что в пространстве foo не 
объявлена переменная a, но в пространстве global, внутри которого находится пространство 
foo, она объявлена. Аналогично, результатом запроса get bar b будет являться foo, а 
результатом работы get bar a будет являться bar.

Результатом get foo c будет являться None, потому что ни в пространстве foo, ни в его
 внешнем пространстве global не была объявлена переменная с.

Более формально, результатом работы get <namespace> <var> является

<namespace>, если в пространстве <namespace> была объявлена переменная <var>
get <parent> <var> – результат запроса к пространству, внутри которого было создано 
пространство <namespace>, если переменная не была объявлена
None, если не существует <parent>, т. е. <namespace>﻿ – это global
Формат входных данных

В первой строке дано число n (1 ≤ n ≤ 100) – число запросов.
В каждой из следующих n строк дано по одному запросу.
Запросы выполняются в порядке, в котором они даны во входных данных.
Имена пространства имен и имена переменных представляют из себя строки длины не более 10, 
состоящие из строчных латинских букв.

Формат выходных данных

Для каждого запроса get выведите в отдельной строке его результат.



Sample Input:
9
add global a
create foo global
add foo b
get foo a
get foo c
create bar foo
add bar a
get bar a
get bar b
Sample Output:
global
None
bar
foo
'''