'''
Напишите реализацию функции closest_mod_5, принимающую в
качестве единственного аргумента целое число x и
возвращающую самое маленькое целое число y, такое что:

y больше или равно x
y делится нацело на 5
Формат того, что ожидается от вас в качестве ответа:

def closest_mod_5(x):
    if x % 5 == 0:
        return x
    return "I don't know :("
'''


def closest_mod_5(x):
    result = 0
    # if x <= 5:
    #     result = 5
    # el
    if x % 5 == 0:
        result = x
    else:
        result = closest_mod_5(x + 1)
    return result
    return "I don't know :("

for i in [-3, -7, 0, 1, 4, 5, 6, 8, 10, 13, 45, ]:
    res = closest_mod_5(i)
    print(i, res)
