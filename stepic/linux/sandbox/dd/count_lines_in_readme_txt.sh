#!/bin/bash

res="summary.info"

if [[ -e $res ]]; then
  rm -v $res
fi

while read line
do
#   echo $line; # здесь можно выполнять любые действия над $line, например, просто напечатать ее
  f=$line"readme.txt"
  echo $f
  if [[ -e $f ]]; then
    cat $f | wc -l >> $res
  else
    echo "-1" >> $res
  fi

done < list.info

