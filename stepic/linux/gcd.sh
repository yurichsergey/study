#!/bin/bash
# Скрипт находит наибольший общий делитель 
# (НОД, greatest common divisor, GCD) двух чисел. 

gcd () # a b
{
  local a=$1
  local b=$2
  #echo  "\na=$a, b=$b, Inner function\n"
  local r=0
  let "r = $a % $b"; #printf "r is $r\n"
  if [[ r -eq 0 ]]; then
    return $b
  else
    gcd $b $r
    return $?
  fi
}


while [[ 1 -eq 1 ]]; do
  read a b
  if [[ $a == '' ]]; then
    break
  fi

  gcd $a $b
  result=$?
  echo "GCD is $result"
done

echo "bye"
exit 0
