#!/bin/bash

#echo "Hello console! I'm count students..."

echo "0 ==> '${0}'; 1 ==> '$1'"

flag=0
#echo "init flag ===> $flag"

flag=1

while [[ 1 -eq 1 ]] # [[ $flag -lt 10 ]]
do
  echo "enter your name:"
  read name
  #echo "your input this string ===> '$name'"
  if [[ -z $name ]]
  then
    #echo "your name is empty => bye"
    break
  #else
    #echo "your name is => $name"
  fi

  
  echo "enter your age:"
  read age
  #echo "your age input this string ===> '$age'"
  if [[ $age -eq 0 ]]
  then
    break
  fi
  
  group=""
  if [[ $age -le 16 ]]
  then
    group="child"
  elif [[ $age -le 25 ]]
  then
    group="youth"
  else
    group="adult"
  fi

  echo "$name, your group is $group"


  flag=$((flag+1))
  #echo "go home ===> $flag"

done

echo "bye"
