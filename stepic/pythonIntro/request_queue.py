'''
Имеется набор файлов, каждый из которых, кроме последнего, содержит
имя следующего файла.
Первое слово в тексте последнего файла: "We".

Скачайте предложенный файл. В нём содержится ссылка на первый файл
из этого набора.

Все файлы располагаются в каталоге по адресу:
https://stepic.org/media/attachments/course67/3.6.3/

Загрузите содержимое ﻿последнего файла из набора, как ответ на это
задание.
'''

import requests


def find_first_word(text):
    result = ''
    for line in text.splitlines():
        words = line.strip().split(' ')
        if words[0]:
            result = words[0]
            break
    return result



def get_url(text):
    result = ''
    for line in text.splitlines():
        result = line.strip()
        break
    return result


def main():
    base_url = 'https://stepic.org/media/attachments/course67/3.6.3/'
    data_quiz = './data/dataset_3378_3_2.txt'
    with open(data_quiz, 'r') as inf:
        url = inf.readline().strip()

    i = 0
    while True:
        i += 1
        file = './data_queue/file_'+str(i)+'.txt'
        if not url:
            print('No url ==> break while')
            break

        r = requests.get(url)
        print('url ==> ', url)
        print('status ==> ', r.status_code)
        if r.status_code != 200:
            break

        text = r.text
        with open(file, 'w') as inf:
            inf.write(text)

        if find_first_word(text) == 'We':
            print('end queue ===> '+file)
            print('===============================')
            print(text)
            print('===============================')
            break

        url = base_url+get_url(text)

main()

'''

ANSWER:


url ==>  https://stepic.org/media/attachments/course67/3.6.3/843785.txt
status ==>  200
end queue ===> ./data_queue/file_1.txt
===============================
We are the champions, my friends,
And we'll keep on fighting 'til the end.
We are the champions.
We are the champions.
No time for losers
'Cause we are the champions of the world.
===============================

'''
