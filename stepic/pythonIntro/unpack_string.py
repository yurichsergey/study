'''

На прошлой неделе мы сжимали строки, используя кодирование повторов. Теперь нашей
задачей будет восстановление исходной строки обратно.

Напишите программу, которая считывает из файла строку, соответствующую тексту,
сжатому с помощью кодирования повторов, и производит обратную операцию, получая исходный текст.

Запишите полученный текст в файл и прикрепите его, как ответ на это задание.

В исходном тексте не встречаются цифры, так что код однозначно интерпретируем.

Sample Input:
a3b4c2e10b1
Sample Output:
aaabbbbcceeeeeeeeeeb

'''


def unpack_string(input_string):
    digits_symbols = [str(++i) for i in range(10)]
    count_repeat = ''
    result = ''
    symbol_repeat = ''
    for symbol in input_string:
        if symbol in digits_symbols:
            count_repeat += symbol
        else:
            if count_repeat != '':
                for i in range(int(count_repeat)):
                    result += symbol_repeat
                count_repeat = ''
            symbol_repeat = symbol
    if count_repeat != '':
        for i in range(int(count_repeat)):
            result += symbol_repeat
    return result


answer = unpack_string('a3f1b4c2e10b1')
print(answer)

inf = open('dataset_3363_2.txt', 'r')
inputString = inf.readline()
inf.close()

answer = unpack_string(inputString)
inf = open('answer.txt', 'w')
inf.write(answer)

