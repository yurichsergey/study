

def f(x):
    result = 0
    if x <= -2:
        result = 1 - pow((x + 2), 2)
    elif (-2 < x and x <= 2):
        result = - x / 2
    else: # 2 < x
        result = pow((x - 2), 2) + 1
    return result


tests = [
    { 'x': 4.5, 'f': 7.25 },
    { 'x': -4.5, 'f': -5.25 },
    { 'x': 1, 'f': -0.5 }
]
for test in tests:
    print(test['f'] - f(test['x']))
