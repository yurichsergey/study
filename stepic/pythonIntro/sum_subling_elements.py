
'''
Напишите программу, на вход которой подаётся прямоугольная матрица в виде последовательности строк,
заканчивающихся строкой, содержащей только строку "end" (без кавычек)

Программа должна вывести матрицу того же размера, у которой каждый элемент в позиции i, j равен 
сумме элементов первой матрицы на позициях (i-1, j), (i+1, j), (i, j-1), (i, j+1). У крайних символов 
соседний элемент находится с противоположной стороны матрицы.

В случае одной строки/столбца элемент сам себе является соседом по соответствующему направлению.

Sample Input 1:
9 5 3
0 7 -1
-5 2 9
end
Sample Output 1:
3 21 22
10 6 19
20 16 -1

Sample Input 2:
1
end
Sample Output 2:
4

'''


a = []
flag = True
while flag:
    row = input()
    if row == 'end':
        break
    row = row.split()
    key = -1
    for n in row:
        key += 1
        row[key] = int(n)
    a.append(row)

# a = [[9, 5, 3], [0, 7, -1], [-5, 2, 9]]
# print a

rowCount = len(a)
columnCount = len(a[0])
sums = [[0 for j in range(columnCount)] for i in range(rowCount)]
for i in range(rowCount):
    for j in range(columnCount):
        for deltaI in range(-1, 2, 2):
            localI = i + deltaI
            if localI < 0:
                localI = rowCount - 1
            if localI >= rowCount:
                localI = 0
            sums[i][j] += a[localI][j]
        for deltaJ in range(-1, 2, 2):
            localJ = j + deltaJ
            if localJ < 0:
                localJ = columnCount - 1
            if localJ >= columnCount:
                localJ = 0
            sums[i][j] += a[i][localJ]
# print sums

for i in range(rowCount):
    for j in range(columnCount):
        print(sums[i][j], end=' ')
    print('')

