'''

Напишите программу, которая принимает на стандартный вход список игр футбольных команд с 
результатом матча и выводит на стандартный вывод сводную таблицу результатов всех матчей.

За победу команде начисляется 3 очка, за поражение — 0, за ничью — 1.

Формат ввода следующий:
В первой строке указано целое число nn — количество завершенных игр.
После этого идет nn строк, в которых записаны результаты игры в следующем формате:
Первая_команда;Забито_первой_командой;Вторая_команда;Забито_второй_командой

Вывод программы необходимо оформить следующим образом:
Команда:Всего_игр Побед Ничьих Поражений Всего_очков

Конкретный пример ввода-вывода приведён ниже.

Порядок вывода команд произвольный.

Sample Input:
3
Зенит;3;Спартак;1
Спартак;1;ЦСКА;1
ЦСКА;0;Зенит;2

Sample Output:
Зенит:2 2 0 0 6
ЦСКА:2 0 1 1 1
Спартак:2 0 1 1 1

'''


import json


def print_arrays_from_string(arr):
    # print(len(arr))
    for line in arr:
        print(line)
    return True


def read_from_console():
    result = []
    n_lines = int(input())
    for i in range(n_lines):
        result.append(input())
    return result


def read_from_file(file_name):
    result = []
    with open(file_name, 'r') as inf:
        is_first_str = True
        for line in inf:
            if is_first_str:
                is_first_str = False
                continue
            result.append(line.replace("\n", ""))
    return result


def parse_input_data_as_array(arr):
    result = []
    for line in arr:
        match = line.split(';')
        result.append(match)
    return result


def set_command_result(arr, command, score):
    if command not in arr:
        arr[command] = {
            'all_games': 0,
            'wins': 0,
            'draws': 0,
            'defeats': 0,
            'score': 0
        }
    arr[command]['all_games'] += 1
    arr[command]['score'] += score
    if score == 3:
        arr[command]['wins'] += 1
    elif score == 1:
        arr[command]['draws'] += 1
    else:
        arr[command]['defeats'] += 1
    return arr


def group_input_data_by_commands(arr):
    result = {}
    for line in arr:
        first_command = line[0]
        first_balls = line[1]
        second_command = line[2]
        second_balls = line[3]

        first_score = 0
        second_score = 0
        if first_balls > second_balls:
            first_score = 3
            second_score = 0
        elif first_balls == second_balls:
            first_score = 1
            second_score = 1
        else:
            first_score = 0
            second_score = 3

        result = set_command_result(result, first_command, first_score)
        result = set_command_result(result, second_command, second_score)

    return result


def prepare_to_print(arr):
    result = []
    for key, line in arr.items():
        result.append(
            key + ':' +
            str(line['all_games']) + ' ' +
            str(line['wins']) + ' ' +
            str(line['draws']) + ' ' +
            str(line['defeats']) + ' ' +
            str(line['score'])
        )
    return result


# input_data = read_from_console()
input_data = read_from_file('./data/football_test.txt')
# print_arrays_from_string(input_data)
# print('')

parse_data = parse_input_data_as_array(input_data)
# print(json.dumps(parse_data, indent=4))

group_data = group_input_data_by_commands(parse_data)
# print('')
# print(json.dumps(group_data, indent=4))

printed_data = prepare_to_print(group_data)
# print('')
# print(json.dumps(printed_data, indent=4))
print_arrays_from_string(printed_data)

