'''
Вам даны три булевых значения: X, Y и Z. Используя только описанные
ниже операции, составьте выражение, которое будет истинно тогда и
только тогда, когда ровно одно из булевых значений (X, Y или Z)
истинно. Разрешены следующие логические операции (от наиболее
приоритетной к наименее приоритетной):

!  - НЕ (отрицание)
&  - И
^  - исключающее ИЛИ (xor, сложение по модулю 2)
|  - ИЛИ

Если вы не уверены в приоритете операций то расставляйте скобки.
'''


def _not(a):
    return not a


def _and(a, b):
    return a and b


def _or(a, b):
    return a or b


def _xor(a, b):
    return a != b


def test_xor():
    data = [
        {'a': 0, 'b': 0, 'result': 0},
        {'a': 0, 'b': 1, 'result': 1},
        {'a': 1, 'b': 0, 'result': 1},
        {'a': 1, 'b': 1, 'result': 0},
    ]
    for test in data:
        a = test['a']
        b = test['b']
        result_true = test['result']
        answer = _xor(a, b)
        print(a, b, result_true, answer, result_true - answer)
    return True


def test_data():
    data = [
        {'i': 0, 'x': 0, 'y': 0, 'z': 0, 'result': 0},
        {'i': 1, 'x': 0, 'y': 0, 'z': 1, 'result': 1},
        {'i': 2, 'x': 0, 'y': 1, 'z': 0, 'result': 1},
        {'i': 3, 'x': 0, 'y': 1, 'z': 1, 'result': 0},
        {'i': 4, 'x': 1, 'y': 0, 'z': 0, 'result': 1},
        {'i': 5, 'x': 1, 'y': 0, 'z': 1, 'result': 0},
        {'i': 6, 'x': 1, 'y': 1, 'z': 0, 'result': 0},
        {'i': 7, 'x': 1, 'y': 1, 'z': 1, 'result': 0},
    ]
    return data


def my_func(x, y, z):
    # ((x^y)^z)&(!((x&y)&z))
    # &(^(^(x, y), y), !(&(&(x, y) z)))
    return _and(_xor(_xor(x, y), z), _not(_and(_and(x, y), z)))


def test_my_func():
    data = test_data()
    for test in data:
        i = test['i']
        x = test['x']
        y = test['y']
        z = test['z']
        result_true = test['result']
        answer = my_func(x, y, z)
        print(i, x, y, z, result_true, answer, result_true - answer)
    return True


test_my_func()
