'''
Given the natural number N. Output the word YES, if number N is the exact power of 2, or NO if otherwise.

Sample Input 1:
1
Sample Output 1:
YES

Sample Input 2:
2
Sample Output 2:
YES

'''


n = int(input())

exact_power_of_2 = False
a = n
while True:
    if a == 1:
        exact_power_of_2 = True
        break
    elif a % 2 == 0:
        a = (a // 2)
    else:
        break

if exact_power_of_2:
    print('YES')
else:
    print('NO')
