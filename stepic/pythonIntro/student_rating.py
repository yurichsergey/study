'''
Имеется файл с данными по успеваемости абитуриентов. Он представляет из
себя набор строк, где в каждой строке записана следующая информация:

Фамилия;Оценка_по_математике;Оценка_по_физике;Оценка_по_русскому_языку

Поля внутри строки разделены точкой с запятой, оценки — целые числа.

Напишите программу, которая считывает файл с подобной структурой и для
каждого абитуриента выводит его среднюю оценку по этим трём предметам
на отдельной строке, соответствующей этому абитуриенту.

Также в конце файла, на отдельной строке, через пробел запишите средние
баллы по математике, физике и русскому языку по всем абитуриентам:

Примечание. Для разбиения строки на части по символу ';' можно использовать
метод split следующим образом:

print('First;Second-1 Second-2;Third'.split(';'))
# ['First', 'Second-1 Second-2', 'Third']

Sample Input:
Петров;85;92;78
Сидоров;100;88;94
Иванов;58;72;85

Sample Output:
85.0
94.0
71.666666667
81.0 84.0 85.666666667
'''


mediumByStudent = []
allScience = [0, 0, 0]
with open('./data/dataset_3363_4.txt', 'r') as inf:
    for line in inf:
        studentData = line.replace("\n", "").split(';')
        name = studentData[0]
        science1 = int(studentData[1])
        science2 = int(studentData[2])
        science3 = int(studentData[3])
        medium = (science1 + science2 + science3) / 3
        mediumByStudent.append(medium)
        allScience[0] += science1
        allScience[1] += science2
        allScience[2] += science3

numberStudents = len(mediumByStudent)
for i in range(len(allScience)):
    allScience[i] /= numberStudents

with open('answer_student_result.txt', 'w') as inf:
    for line in mediumByStudent:
        inf.write(str(line)+"\n")
    allScienceStr = list(map(str, allScience))
    inf.write(' '.join(allScienceStr))


#
# inf.write(answer)
# print(answer)
# '''

