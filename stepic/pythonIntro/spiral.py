
'''
Дополнительная

Выведите таблицу размером n×n, заполненную числами от 1 до n^2 по спирали, 
выходящей из левого верхнего угла и закрученной по часовой стрелке, как показано 
в примере (здесь n=5):

Sample Input:
5
Sample Output:
1 2 3 4 5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9

'''

import sys


fullSize = int(input())

indexIncrease = True # False - Reduction
countIncrease = 0 # 1 - prev direction increas index, 2 - change direction increase index and reset 0
indexXChange = False # True - First Index Change, False - Second Index Change

minX = 0
maxX = fullSize - 1
minY = 0
maxY = fullSize - 1

value = 1

result = [[0 for j in range(fullSize)] for i in range(fullSize)]

#print('minX =', minX, '  maxX =', maxX, '     minY =', minY, '  maxY =', maxY)

i = 0
j = 0 - 1
while True:



    if (minX > maxX and minY > maxY):
        break

    if (indexXChange):
        if (minX == maxX):
            indexXChange = not indexXChange
            countIncrease += 1
            if (countIncrease > 1):
                indexIncrease = not indexIncrease
                countIncrease = 0

        if (indexIncrease):
            if (j == maxY):
                maxY -= 1

            if (i < maxX):
                i += 1
            else:
                indexXChange = not indexXChange
                countIncrease += 1
                if (countIncrease > 1):
                    indexIncrease = not indexIncrease
                    countIncrease = 0
                continue
        else: # index decrease
            if (j == minY):
                minY += 1

            if (i > minX):
                i -= 1
            else:
                indexXChange = not indexXChange
                countIncrease += 1
                if (countIncrease > 1):
                    indexIncrease = not indexIncrease
                    countIncrease = 0
                continue
    else:
        if (minY == maxY):
            indexXChange = not indexXChange
            countIncrease += 1
            if (countIncrease > 1):
                indexIncrease = not indexIncrease
                countIncrease = 0

        if (indexIncrease):
            if (i == minX):
                minX += 1

            if (j < maxY):
                j += 1
            else:
                indexXChange = not indexXChange
                countIncrease += 1
                if (countIncrease > 1):
                    indexIncrease = not indexIncrease
                    countIncrease = 0
                continue
        else: # index decrease
            if (i == maxX):
                maxX -= 1

            if (j > minY):
                j -= 1
            else:
                indexXChange = not indexXChange
                countIncrease += 1
                if (countIncrease > 1):
                    indexIncrease = not indexIncrease
                    countIncrease = 0
                continue

#    print (value, '      ', i, j, '         minX =', minX, '  maxX =', maxX, '     minY =', minY, '  maxY =', maxY)

    result[i][j] = value
    value += 1

# Print Result
for i in range(len(result)):
    for j in range(len(result[0])):
        print(result[i][j], end=' ')
    print('')



#print('Hello')

sys.exit()

