'''
Write a program, which takes the list of numbers and creates a new list, which contains all the elements of this list, excluding duplicates.

Write your solutions as a function f(ls).

Sample Input:
5 10 15 15 25 5
Sample Output:
5 10 25 15
'''


def f(ls):
    uses = {}
    result = []
    for el in ls:
        if not (el in uses):
            result.append(el)
            uses[el] = True
    return result


ls = [5, 10, 15, 15, 25, 5]
print(f(ls))
