
def modify_list(lst):
	result = []
	for n in lst:
		if ((n % 2) == 0):
			result.append(int(n/2));
	lst.clear()
	lst.extend(result)

	return



lst = [1, 2, 3, 4, 5, 6]
print(modify_list(lst))  # None
print(lst)               # [1, 2, 3]
modify_list(lst)
print(lst)               # [1]

lst = [10, 5, 8, 3]
modify_list(lst)
print(lst)               # [5, 4]