'''
Print the sum of all integers from a to b (a < b).

Sample Input:
8
24
Sample Output:
272
'''

start = int(input())
stop = int(input())

result = 0
for i in range(start, stop + 1):
    result += i

print(result)

'''
'''

import sys

print(input().upper())
sys.exit(0)
