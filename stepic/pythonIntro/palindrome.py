'''
Given a string. Find whether it is a palindrome,
i.e. it reads the same both left-to-right and
right-to-left. Output “yes” if the string is
a palindrome and “no” otherwise.

Sample Input:
kayak
Sample Output:
yes
'''


def is_palindrome(string):
    result = True
    len_string = len(string)
    for i in range(len_string//2):
        if string[i] != string[len_string - 1 - i]:
            result = False
            break

    return result


def print_is_palindrome(string):
    if is_palindrome(string):
        print('yes')
    else:
        print('no')
    return True

test_data = ['kayak', 'alibaba', 'qottoq']
for test in test_data:
    print(test)
    print_is_palindrome(test)

