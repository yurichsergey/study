'''
Недавно мы считали для каждого слова количество его вхождений в строку.
Но на все слова может быть не так интересно смотреть, как, например,
на наиболее часто используемые.

Напишите программу, которая считывает текст из файла (в файле может
быть больше одной строки) и выводит самое частое слово в этом тексте
и через пробел то, сколько раз оно встретилось. Если таких слов несколько,
вывести лексикографически первое (можно использовать оператор < для строк).

Слова, написанные в разных регистрах, считаются одинаковыми.

Sample Input:
abc a bCd bC AbC BC BCD bcd ABC
Sample Output:
abc 3

'''


def print_max_element(input_hash):
    freq_keys = []
    max_count = 0
    for k in input_hash:
        # print(input_hash[k])
        if input_hash[k] > max_count:
            max_count = input_hash[k]
            freq_keys.clear()
            freq_keys.append(k)
        elif input_hash[k] == max_count:
            freq_keys.append(k)

    print(freq_keys)
    sorted_freq_keys = sorted(freq_keys)

    max_key = sorted_freq_keys[0] if sorted_freq_keys and sorted_freq_keys.count else None
    max_value = input_hash[max_key] if max_key and max_key in input_hash else None
    result = str(max_key) + ' ' + str(max_value) + "\n"
    return result


def most_frequent_word(input_string):
    words_hash = {}
    for word in input_string.lower().split(' '):
        if word in words_hash:
            words_hash[word] += 1
        else:
            words_hash[word] = 1
    print(words_hash)
    result = print_max_element(words_hash)
    return result

'''
answer = most_frequent_word('abc a bCd bC AbC BC BCD bcd ABC')
print(answer)

'''
input_string = ''
with open('dataset_3363_3_6.txt', 'r') as inf:
    for line in inf:
        input_string += line.replace("\n", "")

answer = most_frequent_word(input_string)
inf = open('answer.txt', 'w')
inf.write(answer)
print(answer)
# '''

