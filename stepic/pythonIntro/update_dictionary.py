
def update_dictionary(dict, key, value):
	if key in dict:
		dict[key].append(value)
	elif int(2*key) in dict:
		dict[int(2*key)].append(value)
	else:
		dict[int(2*key)] = [value]


	return



d = {}
print(update_dictionary(d, 1, -1))  # None
print(d)                            # {2: [-1]}
update_dictionary(d, 2, -2)
print(d)                            # {2: [-1, -2]}
update_dictionary(d, 1, -3)
print(d)                            # {2: [-1, -2, -3]}