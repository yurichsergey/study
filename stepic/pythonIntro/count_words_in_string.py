

def countWords(string):
	result = {}
	strSplit = str(string).lower().split(' ')
	for word in strSplit:
		if word in result:
			result[word] += 1
		else:
			result[word] = 1
	return result

def printHash(inputHash):
	result = ''
	isEnter = False
	for (key, val) in inputHash.items():
		if isEnter:
			result += "\n"
		else:
			isEnter = True
		result += (str(key) + ' ' + str(val))
	return result

#'''
string = input()

countHash = countWords(string)
print(printHash(countHash))

'''

tests = [
	'a aa abC aa ac abc bcd a',
	'a A a',
]
for test in tests:
	print("\n"+test+"\n")
	countHash = countWords(test)
	print(printHash(countHash))	
'''
