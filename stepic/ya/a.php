<?php


main();

function main()
{
    $stdin = fopen('php://stdin', 'r');

    $n = fgets($stdin);

    $k = 1;
    while ($k <= $n) {
        $k++;
        if ($n % $k != 0) {
            echo $k, "\n";
            break;
        }
    }
}

