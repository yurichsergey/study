<?php

main($argv);

function main($argv)
{
    $stdin = fopen('php://stdin', 'r');

    $inputData = fgets($stdin);

//    var_dump($inputData);

    $inputArr = preg_split('/ /', $inputData);

//    var_dump($inputArr);

    $a = (int)$inputArr[0];
    $b = (int)$inputArr[1];
    $c = (int)$inputArr[2];
    $d = (int)$inputArr[3];


//    var_dump([$a, $b, $c, $d]);

    $sum = ($a + $b + $c + $d) / 2;

    $v1 = $a + $c;
    $v2 = $b + $d;

    $w1 = $a + $b;
    $w2 = $c + $d;

    $u1 = $a + $d;
    $u2 = $c + $b;

// echo $a, ' ', $b, ' ', $c, ' ' , $d, "\n";
// echo $s1, ' ', $s2, ' ', $s, "\n";
// echo $ss1, ' ', $ss2, ' ', $s, "\n";
// echo $sss1, ' ', $sss2, ' ', $s, "\n";

    if (
        (eq($v1, $v2) && eq($v1, $sum)) ||
        (eq($w1, $w2) && eq($w1, $sum)) ||
        (eq($u1, $u2) && eq($u1, $sum))
//        (eq($v1, $v2)) ||
//        (eq($w1, $w2)) ||
//        (eq($u1, $u2))
    ) {
        echo "Yes\n";
    } else {
        echo "No\n";
    }
}

function eq($a, $b) {
    //return ($a - $b) < 1e-10;
    return $a == $b;
}
