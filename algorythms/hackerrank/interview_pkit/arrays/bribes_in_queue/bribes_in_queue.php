<?php
/**
 * https://www.hackerrank.com/challenges/new-year-chaos/problem
 */


function minimumBribesOptimal(array $queue): string
{
    $n = count($queue);
    $bribes = 0;
    //echo "\nqueue: " . json_encode($queue) . "\n";
    for ($index = $n - 1; $index >= 0; $index--) {
        $curPos = $index + 1;
        $realPos = $queue[$index];
        if (($realPos - $curPos) > 2) {
            return 'Too chaotic';
        }
        //echo "circle: $index, curPos: $curPos, realPos: $realPos; \n";

        //$bribes += calcBribesInRangeFromHakkerRank($queue, $realPos, $curPos);
        $bribes += calcBribesInRangeMy($queue, $n, $realPos, $curPos);

        ////echo ":: bribes: $bribes\n";
    }
    return (string)$bribes;
}

function calcBribesInRangeMy(array $queue, int $n, int $realPos, int $curPos): int
{
    $bribes = 0;
    for ($j = $n; $j > ($curPos - 1); $j--) {
        //for ($j = max($realPos - 2, 1); $j <= ($curPos); $j++) {
        $index2 = $j - 1;
        $val2 = $queue[$index2];
        //echo "j/i2/v: $j/$index2/$val2,  ";
        if ($realPos > $val2) {
            $bribes++;
        }
    }
    return $bribes;
}

function calcBribesInRangeFromHakkerRank(array $queue, int $realPos, int $curPos): int
{
    $bribes = 0;
    for ($j = max($realPos - 2, 1); $j <= ($curPos); $j++) {
        $index2 = $j - 1;
        $val2 = $queue[$index2];
        //echo "j/i2/v: $j/$index2/$val2,  ";
        if ($val2 > $realPos) {
            $bribes++;
        }
    }
    return $bribes;
}


function testMinimumBribes()
{
    $cases = [
        ['queue' => [2, 1, 5, 3, 4,], 'expectedOut' => 3,],
        ['queue' => [2, 5, 1, 3, 4,], 'expectedOut' => 'Too chaotic',],
        ['queue' => [5, 1, 2, 3, 7, 8, 6, 4,], 'expectedOut' => 'Too chaotic',],
        ['queue' => [1, 2, 5, 3, 7, 8, 6, 4,], 'expectedOut' => 7,],
        ['queue' => [1, 2, 5, 3, 4, 7, 8, 6,], 'expectedOut' => 4,],
    ];
    foreach ($cases as $case) {
        $queue = $case['queue'];
        $expectedOut = $case['expectedOut'];

        //$actual = minimumBribes($queue);
        $actual = minimumBribesOptimal($queue);
        $isOk = $actual === (string)$expectedOut;
        echo ($isOk ? '+' : '-') . ' queue: ' . json_encode($queue)
            . ', expectedOut: ' . $expectedOut
            . ', actualOut: ' . $actual
            . PHP_EOL;
    }
}

function minimumBribes(array $expectedQueue)
{
    $countEls = count($expectedQueue);
    $queue = range(1, $countEls);
    $bribes = 0;
    for ($index = 0; $index < $countEls; $index++) {
        $currentPosition = $index + 1;
        $expectedOrigPosition = $expectedQueue[$index];

        $changedPosition = array_search($expectedOrigPosition, $queue) + 1;
        shiftElInArray($queue, $currentPosition, $changedPosition);

        $div = $changedPosition - $currentPosition;
        if ($div < 3) {
            $bribes += $div;
        } else {
            return 'Too chaotic';
        }
    }
    return $bribes;
}

function shiftElInArray(array &$data, int $i, int $j): void
{
    $minInd = ($i < $j) ? $i : $j;
    $maxInd = ($i < $j) ? $j : $i;
    if ($minInd !== $maxInd) {
        $storedEl = $data[$maxInd - 1];
        for ($a = $maxInd - 1; $a >= $minInd; $a--) {
            $data[$a] = $data[$a - 1];
        }
        $data[$minInd - 1] = $storedEl;
    }
}

function testShiftElInArray(): void
{
    $origData = [1, 2, 3, 4, 5, 6, 7, 8, 9,];
    $cases = [
        [[1, 2, 5, 3, 4, 6, 7, 8, 9,], 3, 5,],
    ];
    foreach ($cases as $case) {
        $assertCase = $case[0];
        $testCase = $origData;
        shiftElInArray($testCase, $case[1], $case[2]);
        echo json_encode($assertCase) . PHP_EOL;
        echo json_encode($testCase) . PHP_EOL;
        var_export($assertCase == $testCase);
    }
}

testMinimumBribes();
//testShiftElInArray();
echo PHP_EOL;

//$stdin = fopen("php://stdin", "r");
//fscanf($stdin, "%d\n", $t);
//for ($t_itr = 0; $t_itr < $t; $t_itr++) {
//    fscanf($stdin, "%d\n", $n);
//    fscanf($stdin, "%[^\n]", $q_temp);
//    $q = array_map('intval', preg_split('/ /', $q_temp, -1, PREG_SPLIT_NO_EMPTY));
//    echo minimumBribes($q) . "\n";
//}
//fclose($stdin);
