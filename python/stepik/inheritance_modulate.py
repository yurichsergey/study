'''
Реализуйте программу, которая будет эмулировать работу с наследованием
классов в python


Вам дано описание наследования классов в следующем формате. 
<имя класса 1> : <имя класса 2> <имя класса 3> ... <имя класса k>
Это означает, что класс 1 отнаследован от класса 2, класса 3, и т. д.

Или эквивалентно записи:

class Class1(Class2, Class3 ... ClassK):
    pass
Класс A является прямым предком класса B, если B отнаследован от A:


class B(A):
    pass


Класс A является предком класса B, если 
A = B;
A - прямой предок B
существует такой класс C, что C - прямой предок B и A - предок C

Например:
class B(A):
    pass

class C(B):
    pass

# A -- предок С


Вам необходимо отвечать на запросы, является ли один класс предком другого
класса

Важное примечание:
Создавать классы не требуется.
Мы просим вас промоделировать этот процесс, и понять существует ли путь от
одного класса до другого.
Формат входных данных

В первой строке входных данных содержится целое число n - число классов.

В следующих n строках содержится описание наследования классов. В i-й строке
указано от каких классов наследуется i-й класс. Обратите внимание, что класс
может ни от кого не наследоваться. Гарантируется, что класс не наследуется
сам от себя (прямо или косвенно), что класс не наследуется явно от одного
класса более одного раза.

В следующей строке содержится число q - количество запросов.

В следующих q строках содержится описание запросов в формате <имя класса 1>
<имя класса 2>.
Имя класса – строка, состоящая из символов латинского алфавита, длины не
более 50.

Формат выходных данных

Для каждого запроса выведите в отдельной строке слово "Yes", если класс 1
является предком класса 2, и "No", если не является. 

Sample Input:
4
A
B : A
C : A
D : B C
4
A B
B D
C D
D A

Sample Output:
Yes
Yes
Yes
No

'''


import json


' ========================================== '


def parse_input_data_as_array(arr):
    result = []
    for line in arr:
        match = line.split(' ')
        result.append(match)
    return result


def print_arrays_from_string(arr):
    # print(len(arr))
    for line in arr:
        print(line)
    return True


def read_from_console():
    result = []
    n_lines = int(input())
    for i in range(n_lines):
        result.append(input())
    return result


def read_from_file(file_name):
    result = []
    with open(file_name, 'r') as inf:
        is_first_str = False
        for line in inf:
            if is_first_str:
                is_first_str = False
                continue
            result.append(line.replace("\n", ""))
    return result


def prepare_input_data_from_file(array_in):
    result = {}
    is_find_count = True
    count_els = 0
    number_array = 0
    read_lines = 0
    for line_str in array_in:
        if count_els <= read_lines:
            is_find_count = True
            count_els = 0
            read_lines = 0
            if number_array in result:
                number_array += 1
        if is_find_count:
            if line_str.isdigit():
                is_find_count = False
                count_els = int(line_str)
            else:
                continue
        else:
            read_lines += 1
            if number_array not in result:
                result[number_array] = []
            result[number_array].append(line_str)

    return result


' ========================================== '


def create_inheritance_model(arr_command):
    classes_dict = {
    }
    for line in arr_command:
        len_line = len(line)
        if len_line > 0:
            namespace = line[0]
            parents = []
            if len_line > 2:
                # in range() second parameter don't return!!
                for i in range(2, len_line):
                    parents.append(line[i])
            classes_dict[namespace] = {
                'parents': parents
            }

    # print(json.dumps(arr_command, indent=4))
    # print(json.dumps(classes_dict, indent=4))
    return classes_dict


def find_in_array(namespaces_dict, namespace, parent):
    result = False
    if namespace in namespaces_dict:
        if parent in namespaces_dict[namespace]['parents']:
            result = namespace
        elif 'None' != parent:
            result = find_in_array(namespaces_dict, parent, parent)
    return result


def test_namespaces(namespaces_tree_dict, data_test):
    for test in data_test:
        namespace = test[0]
        parent = test[1]
        if find_in_array(namespaces_tree_dict, namespace, parent):
            print('Yes')
        else:
            print('No')


# 1. Read Classes tree inheritance
# input_create_namespace = read_from_console()
# input_test_namespace = read_from_console()
input_data = prepare_input_data_from_file(
    read_from_file('./data/inheritance_modulate_IN.dat')
)
# print(input_data)
input_create_namespace = input_data[0]
input_test_namespace = input_data[1]
# print(input_create_namespace)
# print(input_test_namespace)
# print_arrays_from_string(input_create_namespace)
# print_arrays_from_string(input_test_namespace)
# print("\n\n")

# 2. Parse As Array input data
parse_data_create = parse_input_data_as_array(input_create_namespace)
parse_data_test = parse_input_data_as_array(input_test_namespace)
# print(json.dumps(parse_data_create, indent=4))
# print(json.dumps(parse_data_test, indent=4))
# print('')


# 3. Create namespaces tree inheritance
namespaces_tree = create_inheritance_model(parse_data_create)
print(json.dumps(namespaces_tree, indent=4))
print('')
print(json.dumps(parse_data_test, indent=4))
print('')

# 4. Test Namespaces
test_namespaces(namespaces_tree, parse_data_test)
# result_print.append(find_in_array(classes_dict, parents, value))
# for row in result_answer:
#     print(row)

