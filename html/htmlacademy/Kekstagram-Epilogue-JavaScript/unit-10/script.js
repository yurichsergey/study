// var filterName = 'kelvin';
// var filterName = 'toaster';
var filterName = 'walden';

var control = document.querySelector('li.' + filterName);
if (control) {
    control.classList.add('active');
}

var photo = document.querySelector('.photo');
if (photo) {
    photo.classList.add(filterName);
}