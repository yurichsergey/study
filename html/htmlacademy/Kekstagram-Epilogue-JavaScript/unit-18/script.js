var controls = document.querySelectorAll('.toggle-controls li');
var photo = document.querySelector('.photo');

for (var i = 0; i < controls.length; i++) {
    controls[i].innerHTML = controls[i].dataset.filter;
}

function toggleFilter(filterName) {
    for (var i = 0; i < controls.length; i++) {
        controls[i].classList.remove('active');
        photo.classList.remove(controls[i].dataset.filter);
    }

    var control = document.querySelector('li.' + filterName);
    if (control) {
        control.classList.add('active');
    }

    if (photo) {
        photo.classList.add(filterName);
    }
}

var walden = document.querySelector('li.walden');
if (walden) {
    walden.addEventListener('click', function() {
        toggleFilter(walden.dataset.filter);
    });
} 