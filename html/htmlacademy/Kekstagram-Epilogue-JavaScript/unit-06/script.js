var control1 = document.querySelector('li.toaster');
if (control1) {
    control1.classList.remove('active');
}

var control2 = document.querySelector('.walden');
if (control2) {
    control2.classList.add('active');
}

var photo = document.querySelector('.photo');
if (photo) {
    photo.classList.remove('toaster');
    photo.classList.add('walden');
}