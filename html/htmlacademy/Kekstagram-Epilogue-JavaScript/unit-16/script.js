var controls = document.querySelectorAll('.toggle-controls li');

for (var i = 0; i < controls.length; i++) {
    controls[i].innerHTML = controls[i].dataset.filter;
}

function toggleFilter(filterName) {
    for (var i = 0; i < controls.length; i++) {
        controls[i].classList.remove('active');
    }
    
    var control = document.querySelector('li.' + filterName);
    if (control) {
        control.classList.add('active');
    }
    
    var photo = document.querySelector('.photo');
    if (photo) {
        photo.classList.add(filterName);
    }
}

toggleFilter('toaster');
toggleFilter('walden');
toggleFilter('kelvin');