var controls = document.querySelectorAll('.toggle-controls li');
var photo = document.querySelector('.photo');

for (var i = 0; i < controls.length; i++) {
    controls[i].innerHTML = controls[i].dataset.filter;
    // вызов функции clickControl добавить здесь
    clickControl(controls[i]);
}

function toggleFilter(filterName) {
    for (var i = 0; i < controls.length; i++) {
        controls[i].classList.remove('active');
        photo.classList.remove(controls[i].dataset.filter);
    }

    var control = document.querySelector('li.' + filterName);
    if (control) {
        control.classList.add('active');
    }

    if (photo) {
        photo.classList.add(filterName);
    }
}

function clickControl(control) {
    control.addEventListener('click', function() {
        toggleFilter(control.dataset.filter);
    }); 
};