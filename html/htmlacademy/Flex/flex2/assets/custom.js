$(function () {
    var $renderEls = $('.js-render-sample');

    $renderEls.each(function (index, el) {
        var $container = $(el);
        var urlHtml = $container.data('sample-html-url');
        var urlCss = $container.data('sample-css-url');

        var iframe = '<div class="cssbackground-block-item-block">\n' +
            '    <div class="cssbackground-block-item-block__value">\n' +
            '        <div class="block">\n' +
            '            <div style="\n' +
            '            /*display: flex;flex-direction: column;*/\n' +
            '            /*height: 300px;*/\n' +
            '            align-items: center;\n' +
            '                        overflow:hidden;"\n' +
            '                 class="block__row">\n' +
            '                <iframe src="' + urlHtml + '"\n' +
            '                        style="width: 100%; height: 500px; overflow:hidden;"\n' +
            '                ></iframe>\n' +
            '                <!-- height="100%" width="100%"-->\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>\n' +
            '';
        $container.find('.tab__item__js__iframe').prepend(iframe);

        loadSampleCode(urlHtml, $container.find('.tab__item__js__html-code'), 'HTML code');
        loadSampleCode(urlCss, $container.find('.tab__item__js__css-code'), 'CSS code');
    });

    function loadSampleCode(url, $subContainer, label) {
        var jqxhr = $.ajax({
            url: url,
            data: undefined,
            crossDomain: true,
            success: function (response) {
                let code = '' +
                    '<div class="cssbackground-block-item__label">\n' +
                    '    ' + label + ':\n' +
                    '</div>\n' +
                    '<div class="cssbackground-block-item__code">\n' +
                    '<pre>' +
                    '<code>' +
                    '' + $('<div>').text(response).html() + '\n' +
                    '</code>' +
                    '</pre>' +
                    '</div>\n' +
                    '';
                $subContainer.prepend(code);
            },
            dataType: 'text'
        });
    }
});
