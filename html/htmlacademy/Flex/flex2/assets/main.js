$(document).ready((function () {
    $(window).outerWidth();
    $(window).outerHeight();
    var e = window.navigator.userAgent;
    var t = (e.indexOf("MSIE "), {
        Android: function () {
            return navigator.userAgent.match(/Android/i)
        }, BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i)
        }, iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        }, Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i)
        }, Windows: function () {
            return navigator.userAgent.match(/IEMobile/i)
        }, any: function () {
            return t.Android() || t.BlackBerry() || t.iOS() || t.Opera() || t.Windows()
        }
    });

    $(".wrapper").addClass("loaded");

    $(".header-menu__icon").click((function (e) {
        $(this).toggleClass("active"), $(".header-menu").toggleClass("active"), $("body").toggleClass("lock")
    }));

    function r(e) {
        var s = $('a[href="#' + e + '"]').data("vid");
        $(".popup").removeClass("active").hide(), t.any() ? $("body").addClass("lock") : ($("body").css({paddingRight: $(window).outerWidth() - $(".wrapper").outerWidth()}).addClass("lock"), $("header,.pdb").css({paddingRight: $(window).outerWidth() - $(".wrapper").outerWidth()})), history.pushState("", "", "#" + e), "" != s && $(".popup-" + e + " .popup-video__value").html('<iframe src="https://www.youtube.com/embed/' + s + '?autoplay=1"  allow="autoplay; encrypted-media" allowfullscreen></iframe>'), $(".popup-" + e).fadeIn(300).delay(300).addClass("active")
    }

    $.each($("code"), (function (e, t) {
        var s = $(this).html();
        s.replace("<", "&lt;"), s.replace(">", "&gt;"), $(this).html(s), console.log(s)
    }));
    $("body").on("click", ".tab__navitem", (function (e) {
        var t = $(this).index();
        if ($(this).hasClass("parent")) t = $(this).parent().index();
        $(this).hasClass("active") || ($(this).closest(".tabs").find(".tab__navitem").removeClass("active"), $(this).addClass("active"), $(this).closest(".tabs").find(".tab__item").removeClass("active").eq(t).addClass("active"), $(this).closest(".tabs").find(".slick-slider").length > 0 && $(this).closest(".tabs").find(".slick-slider").slick("setPosition"))
    }));
    $.each($(".spoller.active"), (function (e, t) {
        $(this).next().show()
    }));
    $("body").on("click", ".spoller", (function (e) {
        if ($(this).hasClass("mob") && !t.any()) return !1;
        $(this).hasClass("closeall") && !$(this).hasClass("active") && $.each($(this).closest(".spollers").find(".spoller"), (function (e, t) {
            $(this).removeClass("active");
            $(this).next().slideUp(300);
        }));
        $(this).toggleClass("active").next().slideToggle(300, (function (e, t) {
            $(this).parent().find(".slick-slider").length > 0 && $(this).parent().find(".slick-slider").slick("setPosition")
        }))
    }));

}));
