<?php

/**
 * @author Sergey
 */
class ListCityModel {
    
    protected $MaxRowsInQuery = 250000;
//    protected $MaxRowsInQuery = 10000;

    protected $_file;
    protected $_db;

    protected $query;
    protected $cityArray;


    public function __construct($file, $db) {
        $this->_file = $file;
        $this->_db = $db;
    }

    public function Start() {
        $this->queryHeader();
        
        
        
        for($i = 0; $this->getRowArray(); $i++){
            if ($i > 0)
            {
                $this->AddValuesToQuery();
            
                if (($i - 1) % $this->MaxRowsInQuery != 0 ) {
                    $this->query .= ", ";
                }  else {
                    echo "<br>Read $i rows<br>";
                    $this->AddRowsToDatabase();
//                    echo "<hr>";
                    $this->queryHeader();
//                    var_dump($this->cityArray);
//                    set_time_limit(180);
                }
            }
            
//            if($i > $this->MaxRowsInQuery * 10) {
////                die();
//                exit();
//            }
        }
    }
    
    protected function queryHeader() {
        $queryHeader = "INSERT INTO geo_worldcities "
                . "(id, country, city, region, latitude, longitude) VALUES ";
        $this->query = $queryHeader;
    }
    
    protected function getRowArray() {
        $cityString = $this->_file->getRow();
        if ($cityString != false) {
            $this->cityArray = explode(',',$cityString);
            $this->cityArray[2] = $this->_db->EscapeString($this->cityArray[2]); 
            return true;
        } else {
            return false;
        }
    }

    protected function AddValuesToQuery() {
        /**
         * $cityArray[0] = "Country"
         * $cityArray[1] = "City"
         * $cityArray[2] = "AccentCity"
         * $cityArray[3] = "Region"
         * $cityArray[4] = "Population"
         * $cityArray[5] = "Latitude"
         * $cityArray[6] = "Longitude"
         */
        $this->query .= "(NULL, "
            . "'".$this->cityArray[0]."', "
            . "'".$this->cityArray[2]."', "
            . "'".$this->cityArray[3]."', "
            . "'".$this->cityArray[5]."', "
            . "'".$this->cityArray[6]."') ";
    }
    
    protected function AddRowsToDatabase() {
        
//        $res = 
        $this->_db->Query($this->query);
//        echo $this->_db->Error();
//        var_dump($res);
//        echo "<br>" . $this->query ."<br><br>";
        
    }
    

}
