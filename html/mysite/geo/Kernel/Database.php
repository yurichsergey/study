<?php

class Database{

    private $server;
    private $port;
    private $user;
    private $password;
    private $dbName;

    private $_linkIdentifier;
    
    public function __construct()
    {
        $this->ReadParameters();
        
//        echo "<ul>".
//                "<li>".$this->server.":".$this->port."</li>". 
//                "<li>".$this->user."</li>". 
//                "<li>".$this->password."</li>". 
//                "<li>".$this->dbName."</li>".
//            "</ul>"
//        ;
        
        $this->_linkIdentifier = mysqli_connect(
                $this->server.":".$this->port, 
                $this->user, 
                $this->password, 
                $this->dbName);
 
        if ($this->_linkIdentifier->connect_error) {
            die('Ошибка подключения (' . $this->_linkIdentifier->connect_errno . ') '
                . $this->_linkIdentifier->connect_error);
        }
        
//        echo 'Соединение установлено... ' . $mysqli->host_info . "<br>";
        
        mysqli_set_charset($this->_linkIdentifier, "utf8");
    }
    
    protected function ReadParameters() {
        $handle = fopen(Q_PATH.'./Kernel/parameters.yml', 'r');
        while ($str = fgets($handle)) {
            $arr = explode(':', $str);
//            echo "<br><br> \$arr\[0\] = ".$arr[0];
            $arr[1] = trim($arr[1]);
            switch (trim($arr[0])) {
                case 'database_host':
                    $this->server = $arr[1];
                    break;
                case 'database_port':
                    $this->port = $arr[1];
                    break;
                case 'database_name':
                    $this->dbName = $arr[1];
                    break;
                case 'database_user':
                    $this->user = $arr[1];
                    break;
                case 'database_password':
                    $this->password = $arr[1];
                    break;
                default:
                    break;
            }
        }
        fclose($handle);
    }

    
    // Экранирование спец. символов
    public function EscapeString($text)
    {
        return mysqli_real_escape_string($this->_linkIdentifier, $text);
    }
    
    
    // Запрос, возвращает массив в случае успешного выполнения запроса SELECT (и, возможно, SHOW, DESCRIBE или EXPLAIN)
    // True - в случае успешного выполнения запросов UPDATE, INSERT и других запросах (всех кроме SELECT, SHOW, DESCRIBE или EXPLAIN)
    // False - неудача
    public function Query($sql)
    {
        //echo '<br/><br/>'.$sql.'<br/>';
        $result = mysqli_query($this->_linkIdentifier, $sql);
        if($result !== false and $result !== true)
        {
            $arr = array();
            while($row = mysqli_fetch_assoc($result))
                $arr[] = $row;
/*  
            foreach($arr as $key => $val)
            {
                echo '<br/>key = '.$key.' | | | | | val = '.$val.'<br/>';
                foreach($val as $k => $v)
                {
                    echo 'k = '.$k.' | | | | | | | v = '.$v.'<br/>';
                }
            }
            echo 'row = '.$arr[0]['id'].' &nbsp&nbsp&nbsp count(arr) = '.count($arr,1).' &nbsp&nbsp&nbsp count(row) = '.count($row,1).'<br/>';
*/

            // Велосипед !!!! :-) ХА-ХА-ХА
            if(count($arr,0) === 1)
                return $arr[0];
                
            return $arr;
        }
        elseif($result === true)
        {
            //echo 'true <br/>';
            return true;
        }
        elseif($result === false)
        {
            //echo 'false <br/>';
            return false;
        }
    }
    
    public function Error(){
        return mysqli_error($this->_linkIdentifier) ;
    }
}