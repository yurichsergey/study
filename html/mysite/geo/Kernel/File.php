<?php

class File {
    
    private $_file;
    private $_filename;
            
    function __construct($filename,$right) {
        $this->_filename = $filename;
        $this->_file = fopen($filename, $right);
    }

    public function getRow(){
        return fgets($this->_file);
    }
            
    function __destruct() {
        
//       print "<hr>File '".$this->_filename."' close. "
//               . "<br>Destroying " . $this->_file . "<hr>";
       fclose($this->_file);
    }
}