<?php

class Route {

    public static function Start() {
        
        //Route
        $route = explode('/', $_SERVER['REQUEST_URI']);
        //var_dump($route); //die();
        // $route[0] == ''
        // $route[1] == 'geo'
        if ($route[2] == 'search') {
            
            require './Controller/SearchController.php';
            require './Model/PrintModel.php';
            $action = new SearchController();
            
            if ($route[3] == 'coords') {
                
                require './Model/SearchByCoordsCityModel.php';
                $latitude = $route[4];
                $longitude = $route[5];
                $radius = $route[6];
                $action->SearchByCoordsCityAction($latitude, $longitude, $radius);
                
            } else if ($route[3] == 'cities') {
                
                require './Model/SearchByNameCityModel.php';
                $nameCity = $route[4];
                $action->SearchByNameCityAction($nameCity);

            } else {
                
                echo 'ERROR 404';
//                error404();
            }
        } else if ($route[2] == 'list') {
            
            require Q_PATH . '/Kernel/File.php';
            require './worldcities/ListCity.php';
            require './worldcities/ListCityModel.php';
            
            $listCity = new ListCity();
            $listCity->Start();
            
        } else {
                echo 'ERROR 404';
//                error404();
        }


    }

}