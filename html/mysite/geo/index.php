<?php


// PATH KERNEL
define("Q_PATH", dirname(__FILE__));

require './Kernel/Route.php';
require './Kernel/Database.php';

Route::Start();