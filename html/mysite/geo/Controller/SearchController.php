<?php

class SearchController {

    function __construct() {
        
    }

    /**
    * /geo/search/cities - поиск ближайших к гео-координате городов с заданным 
    * радиусом (пример: найти все города, которые находятся не дальше 30 км от 
    * координаты x:y)
    */
    public function SearchByCoordsCityAction($latitude, $longitude, $radius) {
        $model = new SearchByCoordsCityModel($latitude, $longitude, $radius);

        $correct = $model->CorrectInputCoordsCity();
        if(!$correct[0]){
            echo $correct[1] . '<br>';
            return FALSE;
        }
        
        $arr = $model->SearchByCoordsCity();
        if (count($arr) != 0) {
            $printArr = new PrintModel();
            echo "<pre> " . $printArr->FormTableText($arr) . "</pre>";
        } else {
            echo "По запросу Широта = '$latitude' градусов, "
                    . "Долгота = '$longitude' градусов, "
                    . "Радиус круга ='$radius' километров ничего не найдено.";
        }
    }



   /**
    * /geo/search/coords  - поиск гео-координат по имени города (пример: найти 
    * координаты городов, начинающихся с "Моск")
    */
    public function SearchByNameCityAction($nameCity) {
        $model = new SearchByNameCityModel();
        $correct = $model->CorrectInputNameCity($nameCity);
        if(!$correct[0]){
            echo $correct[1] . '<br>';
            return FALSE;
        }
        
        $arr = $model->SearchByNameCity($nameCity);
        if (count($arr) != 0) {
            $printArr = new PrintModel();
            echo "<pre> " . $printArr->FormTableText($arr) . "</pre>";
        } else {
            echo "По запросу '$nameCity' ничего не найдено.";
        }
   }

}
