<?php

/**
 * @author Sergey
 */
class SearchByCoordsCityModel {
    
    private $radiusSphere = 6371; // km - medium radius of the Earth

    private $radiusCircle;
    private $centerLatitude;
    private $centerLongitude;
    private $minLatitude;
    private $maxLatitude;
    private $minLongitude;
    private $maxLongitude;

    public function __construct($latitude, $longitude, $radius)
    {
        $this->radiusCircle = (float)($radius);
        $this->centerLatitude = (float)($latitude);
        $this->centerLongitude = (float)($longitude);
    }

    
    public function CorrectInputCoordsCity() {
        $maxRadius = 500;
        if(abs($this->centerLatitude) >= 90) {
            $arr[0] = false;
            $arr[1] = "Слишком большое по модулю значение широты. "
                . "Широта может иметь значения от -90 до 90 градусов!!";
            return $arr;
        }  else if(abs($this->centerLongitude) >= 180) {
            $arr[0] = false;
            $arr[1] = "Слишком большое по модулю значение долготы. "
                . "Долгота может иметь значения от -180 до 180 градусов!!";
            return $arr;
        }  else if($this->radiusCircle > $maxRadius) {
            $arr[0] = false;
            $arr[1] = "Слишком большой радиус поиска городов. "
                . "Радиус поиска городов не должен быть более '$maxRadius' км!!";
            return $arr;
        }  else if($this->radiusCircle < 0) {
            $arr[0] = false;
            $arr[1] = "Радиус поиска городов не может быть отрицательным числом!!";
            return $arr;
        }  else {
            $arr[0] = true;
            $arr[1] = "";
            return $arr;
        }  
    }
    
    public function SearchByCoordsCity() {
        $db = new Database();
        $this->CalculateMinMaxAngle();
        $query = "SELECT * "
                . "FROM `geo_worldcities` "
                . "WHERE "
                    . "(latitude "
                        . "BETWEEN '" . $this->minLatitude . "' "
                        . "AND '" . $this->maxLatitude . "') "
                    . "AND "
                    . "(longitude "
                        . "BETWEEN '" . $this->minLongitude . "' "
                        . "AND '" . $this->maxLongitude . "') "
//                . "LIMIT 1000"
                ;
//        echo $query.'<br>';
        $arr = $db->Query($query);
//        $db->Error();
        return $arr;

    }

    private function CalculateMinMaxAngle() {
        $deltaLatitude = rad2deg($this->radiusCircle / $this->radiusSphere);
        $this->minLatitude = $this->centerLatitude - $deltaLatitude;
        if ($this->minLatitude < -90){
            $this->minLatitude = -90;
        }
        $this->maxLatitude = $this->centerLatitude + $deltaLatitude;
        if ($this->maxLatitude > +90){
            $this->maxLatitude = +90;
        }
        
        $deltaLongitude = rad2deg(abs( acos( 
                    ( cos($this->radiusCircle / $this->radiusSphere) 
                    - pow( sin(deg2rad($this->centerLatitude)) , 2 ) ) / 
                    pow( cos(deg2rad($this->centerLatitude)) , 2 ) ) ) );
        $this->minLongitude = $this->centerLongitude - $deltaLongitude;
        if ($this->minLongitude < -180){
            $deltaFrom180Longitude = -180 - $this->minLongitude;
            $this->minLongitude = 180 - $deltaFrom180Longitude;
        }
        $this->maxLongitude = $this->centerLongitude + $deltaLongitude;
        if ($this->maxLongitude > +180){
            $deltaFrom180Longitude = $this->maxLongitude - 180;
            $this->maxLongitude = -180 + $deltaFrom180Longitude;
        }
    }
    
    private function DistanceBetweenTwoPointsOnSphere(
                        $latitudeOne, $latitudeTwo,
                        $longitudeOne, $longitudeTwo) 
    {
        return $this->radiusSphere * 
                acos(sin(deg2rad($latitudeOne)) * sin(deg2rad($latitudeTwo))
                    + cos(deg2rad($latitudeOne)) * cos(deg2rad($latitudeTwo))
                    * cos(deg2rad($longitudeOne - $longitudeTwo)));
    }



}
