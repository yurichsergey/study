<?php

/**
 * @author Sergey
 */
class PrintModel {
    
    public function FormTableText($table) {
        $text = "";
        $rowNumber = 0;
        foreach ($table as $row) {
            if($rowNumber++ == 0) {
                $text .= " id,  " .
                    $this->PrintArrayToString(
                        $this->IteratesKeysArray($row));
            }
            $text .= " " . $rowNumber . ",  " . 
                $this->PrintArrayToString($row);
        }
        return $text;
    }
    
    
    
    private function IteratesKeysArray($array) {
        $result = array();
        $i = 0;
        foreach ($array as $key => $value) {
            $result[$i++] = $key;
        }
        return $result;
    }

    
    private function PrintArrayToString($array) {
        $result = "";
        $elementNumber = 0;
        $lengthArray = count($array);
        foreach ($array as $columnName => $value) {
            $elementNumber++;
            if ($columnName != 'id'){
                $result .= $this->PrintValueToString($value, $elementNumber == $lengthArray);
            }
        }
        return $result;
    }
    
    private function PrintValueToString($value, $endOfArray) {
        $result = $value;
        if (!$endOfArray) {
            $result .= ",  ";
        } else {
            $result .= "\n ";
        }
        return $result;
    }
}
