<?php
class SearchByNameCityModel {

    public function CorrectInputNameCity($nameCity) {
        $minLengthNameCity = 3;
        if(strlen($nameCity) < $minLengthNameCity) {
            $arr[0] = false;
            $arr[1] = "Слишком короткое имя города. "
                . "Имя должно быть длинее $minLengthNameCity символов";
            return $arr;
        }  else {
            $arr[0] = true;
            $arr[1] = "";
            return $arr;
        }
        
    }


    public function SearchByNameCity($nameCity) {
        $db = new Database();
        $query = "SELECT * "
                . "FROM `geo_worldcities` "
                . "WHERE city "
                . "LIKE '". $db->EscapeString($nameCity) ."%' "
//                . "LIMIT 1000"
                ;
        $arr = $db->Query($query);
        return $arr;
    }

    
    
    
    protected function printArray($query)
    {
        foreach ($query as $k0 => $v0)
        {
            if($k0 == 0)
            {
                echo "<br/> ====================";
                echo "<br/> k0 = " . $k0;
                echo "<br/> v0 = " . $v0;
                echo "<br/> ====================";
                foreach ($v0 as $k1 => $v1)
                {
                    echo "<br/> k1 = " . $k1;
                    echo "<br/> v1 = " . $v1 . "<br/>";
                }
            }
        }
    }
	
	
}
