<?php
class ModelText
{
	public function Get()
	{
		return file_get_contents('data/text.txt');
	}

	public function Set($text)
	{
		file_put_contents('data/text.txt', $text);
	}
}