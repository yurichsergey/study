<?php
class C_Edit extends C_Base 
{
	private $_text;
	
	public function __construct()
	{
	}
	
	// Запрос пользователя
	protected function OnInput()
	{
		parent::OnInput();
		
		$model = new ModelText();
		
		if($this->IsPost())
		{
			$text = $_POST['text'];
			$model->Set($text);
			header('Location: index.php?c=view');
			exit();
		}
		
		$this->_text = $model->Get();
		$this->_title .= ' :: Редактирование';
	}
	
	// Ответ сервера
	protected function OnOutput()
	{
		$vars = array('text' => $this->_text);
		$this->_content = $this->Template('v/v_edit.php', $vars);
		parent::OnOutput();
	}
}