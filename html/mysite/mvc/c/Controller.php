<?php
class Controller
{
	public function __construct()
	{
	}
	
	public function Request()
	{
		$this->OnInput();
		$this->OnOutput();
	}
	
	// Запрос пользователя
	protected function OnInput()
	{
	}
	
	// Ответ сервера
	protected function OnOutput()
	{
	}
	
	protected function IsGet()
	{
		return $_SERVER['REQUEST_METHOD'] == 'GET';
	}
	
	protected function IsPost()
	{
		return $_SERVER['REQUEST_METHOD'] == 'POST';
	}
	
	// Генерация html
	// t - файл шаблона
	// vars - ассоциативный массив пеменных
	protected function Template($t, $vars)
	{

		/*  Дальше одно и тоже двумя способами:
		// Создадим переменную x равную 10
		$x = 10;

		// Создадим другим способом переменную x равную 10
		$varName = 'x';
		$$varName = 10;
		*/
	
//	echo '<br/><br/><br/><hr/><hr/>';
		foreach($vars as $key => $value)
		{
			$$key = $value;
			//echo $key.'&nbsp&nbsp&nbsp&nbsp'.$value.'<br/><br/>';
		}
			
		ob_start();		// Буфер
		include($t);		// Подключение шаблона
		return ob_get_clean();	// Строка из буфера
	}
	
}