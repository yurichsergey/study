<?php
class C_Base extends Controller
{
	protected $_title;
	protected $_content;

	public function __construct()
	{
	}
	
	// Запрос пользователя
	protected function OnInput()
	{
		$this->_title = 'Название сайта';
		$this->_content = '';
	}
		
    // Выбор оформления
    private function choise_view()
    {
        if(isset($_GET['theme']))
        {
            $theme = (int)$_GET['theme'];
            if($theme === 1 or $theme === 2)
            {
                $_SESSION['theme'] = $theme;
                $_COOKIE['theme'] = $theme;
                return $theme;
            }
        }
        if(isset($_SESSION['theme']))
            return (int)$_SESSION['theme'];
        if(isset($_COOKIE['theme']))
            return (int)$_COOKIE['theme'];
        return 1;
    }
	
	// Ответ сервера
	protected function OnOutput()
	{
		$vars = array(
			'title' => $this->_title,
			'content' => $this->_content,
		);


		// Подключение представления страницы
        $theme = $this->choise_view();
		if($theme === 1)
			$page = $this->Template('v/v_main_1.php', $vars);
		elseif($theme === 2)
			$page = $this->Template('v/v_main_2.php', $vars);
        else
            $page = $this->Template('v/v_main_1.php', $vars);

		// Альтернатива подключения представления страницы (умолчание в choise_view())
        //$page = $this->Template('v/v_main_'.$this->choise_view().'.php', $vars);
		echo $page;
	}
}