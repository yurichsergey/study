<?php
class C_View extends C_Base 
{
	private $_text;
	
	public function __construct()
	{
	}
	
	// Запрос пользователя
	protected function OnInput()
	{
		parent::OnInput();
		
		$model = new ModelText();
		$this->_text = $model->Get();
        $this->_text = nl2br($this->_text);
        $this->_title .= ' :: Чтение';
	}
	
	// Ответ сервера
	protected function OnOutput()
	{
		$vars = array('text' => $this->_text);
		$this->_content = $this->Template('v/v_view.php', $vars);
/*		echo '<br/>!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<br/>';
		echo '!!!!!!!!!!!!!!!!!!!! $this->_text !!!!!!!!!!!!!!!!!!!!!!! '.$this->_text .'<br/>';
		echo '!!!!!!!!!!!!!!!!!!!! $this->_title !!!!!!!!!!!!!!!!!!!!!!! '.$this->_title .'<br/>';
		echo '!!!!!!!!!!!!!!!!!!!! $this->_content !!!!!!!!!!!!!!!!!!!!!!! '.$this->_content .'<br/>';
*/		parent::OnOutput();
	}
}