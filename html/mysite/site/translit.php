﻿
<?php
mb_language('uni'); 
mb_internal_encoding('UTF-8');			
///////////////////////////////////////////////////////////////////

function translit($str)
{
    // Система транслитерации, соответсвующая стандарту ISO 9:1995 Система Б.
    // echo "<p>Система транслитерации, соответсвующая стандарту ISO 9:1995 Система Б.</p>";

    $aRu["а"] = "a";
    $aRu["б"] = "b";
    $aRu["в"] = "v";
    $aRu["г"] = "g";
    $aRu["д"] = "d";
    $aRu["е"] = "e";
    $aRu["ё"] = "yo";
    $aRu["ж"] = "zh";
    $aRu["з"] = "z";
    $aRu["и"] = "i";
    $aRu["й"] = "j";
    $aRu["к"] = "k";
    $aRu["л"] = "l";
    $aRu["м"] = "m";
    $aRu["н"] = "n";
    $aRu["о"] = "o";
    $aRu["п"] = "p";
    $aRu["р"] = "r";
    $aRu["с"] = "s";
    $aRu["т"] = "t";
    $aRu["у"] = "u";
    $aRu["ф"] = "f";
    $aRu["х"] = "x";
    $aRu["ц"] = "c";
    $aRu["ч"] = "ch";
    $aRu["ш"] = "sh";
    $aRu["щ"] = "shh";
    $aRu["ъ"] = "''";
    $aRu["ы"] = "y'";
    $aRu["ь"] = "'";
    $aRu["э"] = "e'";
    $aRu["ю"] = "yu";
    $aRu["я"] = "ya";

    $aRu["А"] = "A";
    $aRu["Б"] = "B";
    $aRu["В"] = "V";
    $aRu["Г"] = "G";
    $aRu["Д"] = "D";
    $aRu["Е"] = "E";
    $aRu["Ё"] = "YO";
    $aRu["Ж"] = "ZH";
    $aRu["З"] = "Z";
    $aRu["И"] = "I";
    $aRu["Й"] = "J";
    $aRu["К"] = "K";
    $aRu["Л"] = "L";
    $aRu["М"] = "M";
    $aRu["Н"] = "N";
    $aRu["О"] = "O";
    $aRu["П"] = "P";
    $aRu["Р"] = "R";
    $aRu["С"] = "S";
    $aRu["Т"] = "T";
    $aRu["У"] = "U";
    $aRu["Ф"] = "F";
    $aRu["Х"] = "X";
    $aRu["Ц"] = "C";
    $aRu["Ч"] = "CH";
    $aRu["Ш"] = "SH";
    $aRu["Щ"] = "SHH";
    $aRu["Ъ"] = "''";
    $aRu["Ы"] = "Y'";
    $aRu["Ь"] = "'";
    $aRu["Э"] = "E'";
    $aRu["Ю"] = "YU";
    $aRu["Я"] = "YA";

    $aRu[" "] = " ";
    $aRu["."] = ".";
    $aRu[","] = ",";
    $aRu["\\"] = "\\";
    $aRu["/"] = "/";
    $aRu["\""] = "\"";
    $aRu[":"] = ":";
    $aRu[";"] = ";";
    $aRu["["] = "[";
    $aRu["]"] = "]";
    $aRu["("] = "(";
    $aRu[")"] = ")";
    $aRu["<"] = "<";
    $aRu[">"] = ">";
    $aRu["@"] = "@";
    $aRu["!"] = "!";
    $aRu["?"] = "?";
    $aRu["~"] = "#";
    $aRu["+"] = "+";
    $aRu["-"] = "-";
    $aRu["#"] = "#";
    $aRu["%"] = "%";
    $aRu["^"] = "^";
    $aRu["&"] = "&";
    $aRu["*"] = "*";
    $aRu["\n"] = "\n";
    $aRu["\t"] = "\t";
    $aRu["$"] = "$";
    $aRu["00"] = "0";
    $aRu["1"] = "1";
    $aRu["2"] = "2";
    $aRu["3"] = "3";
    $aRu["4"] = "4";
    $aRu["5"] = "5";
    $aRu["6"] = "6";
    $aRu["7"] = "7";
    $aRu["8"] = "8";
    $aRu["9"] = "9";
    $aRu["_"] = "_";
    
    $aRu["a"] = "a";
    $aRu["b"] = "b";
    $aRu["c"] = "c";
    $aRu["d"] = "d";
    $aRu["e"] = "e";
    $aRu["f"] = "f";
    $aRu["g"] = "g";
    $aRu["h"] = "h";
    $aRu["i"] = "i";
    $aRu["j"] = "j";
    $aRu["k"] = "k";
    $aRu["l"] = "l";
    $aRu["m"] = "m";
    $aRu["n"] = "n";
    $aRu["o"] = "o";
    $aRu["p"] = "p";
    $aRu["q"] = "q";
    $aRu["r"] = "r";
    $aRu["s"] = "s";
    $aRu["t"] = "t";
    $aRu["u"] = "u";
    $aRu["v"] = "v";
    $aRu["w"] = "w";
    $aRu["x"] = "x";
    $aRu["y"] = "y";
    $aRu["z"] = "z";

    $aRu["A"] = "A";
    $aRu["B"] = "B";
    $aRu["C"] = "C";
    $aRu["D"] = "D";
    $aRu["E"] = "E";
    $aRu["F"] = "F";
    $aRu["G"] = "G";
    $aRu["H"] = "H";
    $aRu["I"] = "I";
    $aRu["J"] = "J";
    $aRu["K"] = "K";
    $aRu["L"] = "L";
    $aRu["M"] = "M";
    $aRu["N"] = "N";
    $aRu["O"] = "O";
    $aRu["P"] = "P";
    $aRu["Q"] = "Q";
    $aRu["R"] = "R";
    $aRu["S"] = "S";
    $aRu["T"] = "T";
    $aRu["U"] = "U";
    $aRu["V"] = "V";
    $aRu["W"] = "W";
    $aRu["X"] = "X";
    $aRu["Y"] = "Y";
    $aRu["Z"] = "Z";

    
    $string_translit = ""; 
    
    $len = mb_strlen($str);
//                echo "len = " , $len," <br/>";
    
    for( $i = 1; $i <= $len; $i++)
    {
        $a = mb_substr($str,$i-1,1);   
        
        $flag = false;
        foreach($aRu as $vRu => $vEn)
        {
//                        echo "<br/> $i ~ $a == $vRu --> $vEn";
            if ( $a == $vRu )
            {
                $string_translit .= $vEn;
                $flag = true;
                break;
            }
        }
//					if($flag == false){	$string_translit .= "_";}
//					if($flag == false){	$string_translit .= $a;}
    }
    
    return $string_translit;

} 

function fnrows($text, $ncols)
{
    /* Количество строчек для текста, чтобы он полностью поместился */
    $nr = 0;    // количество строк
    $icol = 0;  // количесвто колонок на текущей строке
    $iword = 0; // количество символов в слове 
                // Слово состоит из любых символах кроме переноса строки, пробела и табуляции 
    $len = mb_strlen($text);
    for( $i = 1; $i <= $len; $i++)
    {
        $flag = false;
        $a = mb_substr($text,$i-1,1);
        
        if($a === "\n")
        {
            $nr++;
            if($icol > $ncols) $nr += floor($icol / $ncols);
            $icol = 0;
            $iword = 0;
        }

        $icol++;
        
        if ( $a===" " && $a==="\t")
        {
            if($icol > $ncols) 
            {
                $nr += floor($icol / $ncols);
                $icol = $icol - floor($icol / $ncols);
            }
            $iword = 0;
        }
        else
        {   $iword++;   }
    }
    return $nr+=5;
}

?>
			
<!DOCTYPE html Public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
		<title> Транслитерция русского тескта </title>
	</head>
	<body>
		<table align="center" width="960" cellpadding="2"> <tr>
            <td align="left"><a href="../index.php"><h2>Главная страница</h2></a></td>
            <td align="right"><a href="../all_ex.php"><h2>Все упражнения</h2></a></td></tr>
        </table>
		<table align="center" width="960" cellpadding="2"> <!--Макет страницы-->
			<tr> <!---Заголовок-->
				<td colspan="2">
					<h2>Транслитерация русского тескта </h2>
                    <p>Система транслитерации, соответсвующая стандарту ISO 9:1995 Система Б.</p>
				</td>
			</tr>
			<tr>
                <?php
                    if (isset($_POST['text'])) 
                        {   $text = $_POST['text']; /* Была отправка формы (POST) */ }
                    else 
                        {  /* Обычный запрос страницы (GET) */  }
                    
                    $text_tr = translit($text);
                    $ncols = 55;
                    $nrows = max( fnrows($text, $ncols) , fnrows($text_tr, $ncols) , 25 );
                ?>
				<td width="480" align="left" valign="top" > 
                    <form method="post" action="">
                        <textarea name="text" cols="<?php echo $ncols; ?>" rows="<?php echo $nrows; ?>" placeholder="Введите текст на русском языке....." autofocus><?php echo $text; ?></textarea>
                        <p><input type="submit" value="Отправить">  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp 
                            <a href="translit.php"> Обновить </a>  &nbsp  &nbsp или &nbsp  &nbsp  <a href="../index.php"> Вернуться </a></p>
                    </form>
				</td>
				<td width="480" align="right" valign="top" >
                    <textarea name="text" cols="<?php echo $ncols; ?>" rows="<?php echo $nrows; ?>" placeholder="Здесь будет текст в транслитерации....." ><?php echo "$text_tr";?></textarea>
				<td/>
			</tr>
            <tr>
            </tr>
		</table>
	</body>
</html>
