<div class="imageBig">

    <a class="prev"  href="<?=$_SERVER['PHP_SELF']?>?id=<?=$prev?>">
        <img src="../assets/gallery/icons/ic_navigate_before_black_24px.svg" alt="Назад" /></a>
    <a class="close" href="./index.php">
        <img src="../assets/gallery/icons/ic_close_black_24px.svg" alt="Закрыть" /></a>
    <a class="next"  href="<?=$_SERVER['PHP_SELF']?>?id=<?=$next?>">
        <img src="../assets/gallery/icons/ic_navigate_next_black_24px.svg" alt="Вперед"  /></a>

    <?php $image = gallery_item($id)?>

    <a class="big" href="<?=$_SERVER['PHP_SELF']?>?id=<?=$next?>">
        <img src="<?=gallery_image($image, $path_big)?>" alt="<?=$image['id']?>"/>
    </a>
</div>


<div class="popul">Фотографию посмотрели <b><?=$image['popul']?></b> раз</div>

