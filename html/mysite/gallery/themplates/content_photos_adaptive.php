<header>
	<a name="top"></a>

	<ul class="menu"><!---Заголовок-->
		<li><a href="/">Главная страница</a></li>
		<li></li>
	</ul>
	
	<h1>Галерея фотографий</h1>
</header>

<main>


	<ul class="menu-gallery" >
		<li class="">
			<a class="" href="#"><?=$menuSortType['title'] ?></a>
			<ul class="sub-menu">
				<li><a href="<?=$menuSortType['address'] ?>"><?=$menuSortType['sub-item']?></a></li>
			</ul>
		</li>
		<li class="">
			<a class="" href="#"><?=$menuSortDirection['title'] ?></a>
			<ul class="sub-menu">
				<li><a href="<?=$menuSortDirection['address'] ?>"><?=$menuSortDirection['sub-item'] ?></a></li>
			</ul>
		</li>

	</ul>

	<form class="" method="post" enctype="multipart/form-data">
		<input type="file" name="fname" id="select-file"> 
		<label class="select-file" for="select-file">Выбрать</label>
		<input type="submit" value="Загрузить изображение!" id="post"/>
		<label class="post" for="post">Загрузить</label>
		<p>Только: GIF, PNG, JPEG, XBM, WBMP. Файл не более <?=$post_max_size?>.</p>
	</form>	
			

			
			
	<?php/// Информационное сообщение / Была отправка формы (POST) /
	// if (isset($_GET['message'])) {   echo $_GET['message'];} 
	// $_SESSION['message'][0] - Основное информационное сообщение (ошибка, успех...)
	// $_SESSION['message'][1] - Примечание к сообщению   ?>
	<?php if (isset($_SESSION['message'][0])): ?>
		<div class="message"><?=$_SESSION['message'][0]?>
			<?unset($_SESSION['message'][0])?>
			<a class="message-close" href="<?=$_SERVER['PHP_SELF'] ?>" title="Закрыть сообщение">X</a>
		
			<?php if (isset($_SESSION['message'][1])): ?>
				<?php if ($_SESSION['message'][1] !== ''): ?>
					<p class=""><?=$_SESSION['message'][1]?></p>
					<?php unset($_SESSION['message'][1])?>
				<?php endif?>
			<?php endif?>
		</div>
                <?php unset ($_SESSION['message']); ?>
	<?php endif?>
	
	
	<!-- Печать галереи фото  -->
	<?php if(count($list) !== 0):?>
		<div class="photo_spaces">
		<?php foreach($list as $i => $image):?>
			<div class="photo_frame">
				<a href="image.php?id=<?=$image['id']?>" style="background-image: url('<?=gallery_icon($image, $path_small)?>')">
				<?php /* <a href="image.php?id=<?=$image['id']?>" >
					<img alt="Picture # <?=$image['id']?>" src="<?=gallery_icon($image, $path_small)?>"> */?>
				</a>
				
				<div class="">
				</div>
				
			</div>
		<?php endforeach?>
		</div>
	<?php else:?>
		<h1>Галерея пуста. Добавьте свои фотографии.</h1>
	<?php endif?>
	
	<a class="to-top" href="<?=$_SERVER['PHP_SELF']?>?#top">
            <!--Наверх-->
                <img src="../assets/gallery/icons/ic_upward_black_24px.svg" /></a>
</main>