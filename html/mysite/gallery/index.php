<?php 
////////////////////////////////////////////////////////////////
// Настройки php, для работы с unicode и прочего...
require('settings.php');
 
////////////////////////////////////////////////////////////////
// Подключение модели (библитоеки)
require('model/gallery.php');

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// Функция определения максимально размера загружаемого файла на сервер по метоеду POST
//
// Возвращет строку
//
function return_post_max_size() {
    $val = ini_get('post_max_size');
    $val = trim($val);
    $val = mb_convert_case($val, MB_CASE_LOWER);
    $last = mb_substr($val,mb_strlen($val)-1,1);
    $val = trim( mb_substr($val,0,mb_strlen($val)-1) ); /// Оставим только число
    switch($last) {
        // Модификатор 'G' доступен, начиная с PHP 5.1.0
        case 'g':
            return $val . ' ГБ';
        case 'm':
            return $val . ' МБ';
        case 'k':
            return $val . ' КБ';
        default:
            return $val . ' Б';
    }
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// ТОЧКА ВХОДА (Начало php-скрипта)
//
////////////////////////////////////////////////////////////////

session_start();

// Пути к хранилищу изображений
// include('settings_path.php');
////////////////////////////////////////////////////////////////
// Проверка существования каталогов для хранения файлов
// if (!file_exists($path_big)) {   mkdir($path_big, 0755);}
// if (!file_exists($path_small)) { mkdir($path_small, 0755);}

////////////////////////////////////////////////////////////////
// Проверка, был ли POST запрос, если был, то загрузить фото
if (isset($_FILES['fname'])) 
{
    $message = gallery_add($_FILES['fname']['tmp_name'], $_FILES['fname']['name']);
    $_SESSION['message'][0] = $message[1];  // $message[1] - Основное информационное сообщение (ошибка, успех...)
    $_SESSION['message'][1] = $message[2];  // $message[2] - Примечание к сообщению

    // Перезагрузим страницу
    header("Location: ". $_SERVER['PHP_SELF']);
    exit();
}

// максимальный размер загружаемого файла на сервер по методу POST (тип - string)
$post_max_size = return_post_max_size();

// Получение списка файлов
if($_GET['sort-type'] === 'date')
    $_SESSION['sort-type'] = 'date';
elseif($_GET['sort-type'] === 'popul')
    $_SESSION['sort-type'] = 'popul';
elseif(!isset($_SESSION['sort-type']))
    $_SESSION['sort-type'] = 'popul';

if($_GET['sort-direction'] === 'asc')
    $_SESSION['sort-direction'] = 'asc';
elseif($_GET['sort-direction'] === 'desc')
    $_SESSION['sort-direction'] = 'desc';
elseif(!isset($_SESSION['sort-direction']))
    $_SESSION['sort-direction'] = 'desc';

$list = gallery_list($_SESSION['sort-type'] , $_SESSION['sort-direction'] );

// 
unset($_SESSION['list_id']);
foreach($list as $i => $image)
    $_SESSION['list_id'][$i] = $image['id']; // запись порядка файлов в массив

/// Menu
if($_SESSION['sort-type'] === 'date'){
	$menuSortType['title'] = 'По дате добавления'; 
	$menuSortType['address'] = $_SERVER['PHP_SELF']."?sort-type=popul";
	$menuSortType['sub-item'] = 'По популярности';
} else {
	 $menuSortType['title'] = 'По популярности';
	 $menuSortType['address'] = $_SERVER['PHP_SELF']."?sort-type=date";
	 $menuSortType['sub-item'] = 'По дате добавления';
}
if($_SESSION['sort-direction'] === 'asc'){
	 $menuSortDirection['title'] = 'По возрастанию';
	 $menuSortDirection['address'] = $_SERVER['PHP_SELF'].'?sort-direction=desc';
	 $menuSortDirection['sub-item'] = 'По убыванию';
} else {
	 $menuSortDirection['title'] = 'По убыванию';
	 $menuSortDirection['address'] = $_SERVER['PHP_SELF'].'?sort-direction=asc';
	 $menuSortDirection['sub-item'] = 'По возрастанию';
}

// Подключение пути с отображением    
// if($_SESSION['view'] === 'list')
//     $content = 'themplates/content_index_list.php';
// elseif($_SESSION['view'] === 'table')
//     $content = 'themplates/content_index_table.php';

$content = 'themplates/content_photos_adaptive.php';

// Подключение представления
$title = 'Галерея фотографий';
require('themplates/main.php');