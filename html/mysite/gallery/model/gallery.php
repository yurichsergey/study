<?php 
////////////////////////////////////////////////////////////////
// Файл модели галлереи

////////////////////////////////////////////////////////////////
// Подключение к БД
function my_connect_db()
{
    $db = mysqli_connect('127.0.0.1:3306', 'root', '', 'helloworld_product');
    mysqli_set_charset($db, "utf8");
    return $db;
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// 
// image_resize - Функция сохранения копии изображения
//
// Функция уменьшения изображения и сохранения его на диске
// (Используется при добавлении фото в галерею)
// 
// Функция уменьшает изображения так, чтобы максимальный размер 
// нового изображения по горизонтали или вертикали не превышал 
// $MaxPix  и сохраняет в $file_path_new
// 
// $file_path_new - путь к сохраняемому изображению
// $file_path - путь к исходному изображению
// $imagewidth - ширины исходного изображения
// $imageheigth - высота исоходного изображения
// $imagetype - тип изображения
// $MaxPix - максимальный размер нового изображения по горизонтали или вертикали
//
// image_resize() ничего не возвращает
//
// В image_resize() не используется getimagesize() для оптимизации,
// т.к. эта информация доступна из функции в которой она вызывается 
//
function image_resize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, $MaxPix)
{
	$image_src = call_user_func('imagecreatefrom' . $imagetype, $file_path);
	if($imagewidth <= $MaxPix && $imageheigth <= $MaxPix)
	{
		$new_heigth = $imageheigth;
		$new_width = $imagewidth;
	}
	else
	{
		// Уменьшение размера изображения до $MaxPix по максимальной стороне
// 		if($imagewidth > $imageheigth)
// 		{
// 			$new_heigth = ($imageheigth * $MaxPix / $imagewidth);
// 			$new_width = $MaxPix;
// 		}
// 		else
// 		{
// 			$new_heigth = $MaxPix;
// 			$new_width = ($imagewidth * $MaxPix / $imageheigth);
// 		}
		// Уменьшение размера изображения до $MaxPix по минимальной стороне
		if($imagewidth > $imageheigth)
		{
			$new_heigth = $MaxPix;
			$new_width = (integer)($imagewidth * $MaxPix / $imageheigth);
		}
		else
		{
			$new_heigth = (integer)($imageheigth * $MaxPix / $imagewidth);
			$new_width = $MaxPix;
		}
	}
// 	echo"\$maxPix = $MaxPix,  \$new_heigth = $new_heigth,  \$new_width = $new_width <br>";
	$image_dst = imagecreatetruecolor($new_width, $new_heigth);
	if(!imagecopyresampled($image_dst, $image_src, 0, 0, 0, 0, $new_width, $new_heigth, $imagewidth, $imageheigth))
	{
		header('Location:index.php');
		exit();
	}

	switch($imagetype):	  // IMAGETYPE_XXX
		case "jpeg":
			imagejpeg($image_dst, $file_path_new, 90);
			break;
		default:
			call_user_func('image' . $imagetype, $image_dst, $file_path_new);
			break;
	endswitch;
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// Функция обработки загруженного изображения
// Добавляет файл в галерею
//
// Тип файла определяется из свойств самого файла
//
// $message[0] - true or false
// $message[1] - Основное информационное сообщение (ошибка, успех...)
// $message[2] - Примечание к сообщению
// 
//  Возвращает в случае успеха:  true
//  
function gallery_add($file_path, $file_name)
{	
	// Проверим, пустой ли запрос
	if ($file_name === '' or $file_path === ''){
		$message[0] = false;
		$message[1] = 'Вначале выберите файл!';
		$message[2] = '';
		return $message; }

	// Проверка типа файла
	// Тип файла определяется из свойств самого файла
	$imageinfo = getimagesize($file_path);
	$imagewidth = $imageinfo[0];	// ширина
	$imageheigth = $imageinfo[1];   // высота
	$imagetype = $imageinfo[2];
	switch($imageinfo[2]):	  // IMAGETYPE_XXX
		case IMAGETYPE_PNG:	 $imagetype = "png";	 break;
		case IMAGETYPE_GIF:	 $imagetype = "gif";	 break;
		case IMAGETYPE_JPEG:	$imagetype = "jpeg";	break;
		case IMAGETYPE_JPEG2000: $imagetype = "jpeg";   break;  // ???? 
		case IMAGETYPE_WBMP:	$imagetype = "wbmp";	break;
		case IMAGETYPE_XBM:	 $imagetype = "xbm";	 break;
		default:
			$message[0] = false;
			$message[1] = 'Файл "' . $file_name . '" не загружен! Неизвестный тип файла!';
			$message[2] = 'Пожалуста, загружайте только изображения следующих форматов: GIF, PNG, JPEG, WBMP, XBM!';
			return $message; 
	endswitch;
	
	// Подключение к БД.
	$db = my_connect_db();

	// Идентификатор фото в хранилище (его создание)
	$newname = microtime(true); // Время в микросекундах :)
	$newname = $newname - (int)$newname;
	$dateimage = date('Y M d D H-i-s') . sprintf("-%f",$newname);
	$newname = $dateimage . ' ' . $file_name;	//	echo $newname . '<br/>';
	$newname = md5($newname);
//	while(file_exists($newname)) {   $numberfiles++;   $newname = './big/'. $numberfiles .'.jpg';  }

	// Пути к хранилищу изображений
	include('settings.php');
	
	// Сохранение файла в хранилище больших снимков и создание эскиза
	$file_path_new = $path_big.$newname.'.'.$imagetype;
	image_resize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, 2000);
	$file_path_new = $path_small.$newname.'.'.$imagetype;
	image_resize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, 350);
	
	// sql
	$sql = "INSERT INTO gallery_photo (id, name, type, popul, time) VALUES (NULL, '$newname', '$imagetype', '0', '$dateimage')";
	$qw = mysqli_query($db, $sql);
	if($qw === false){
		$message[0] = false;
		$message[1] = 'Неизвестная ошибка при обращении к базе данных';
		$message[2] = '';
		return $message; }
		
	// отчет об успехе добавления файла в галерею
	$message[0] = true;
	$message[1] = 'Файл "' . $file_name . '" успешно загружен!';
	$message[2] = '';
	return $message;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Функция возвращает объект фотографии (ассоциативный массив)
function gallery_item($id)
{	
	// Подключение к БД.
	$db = my_connect_db();
	$id = (int)$id;
	$result = mysqli_query($db, "SELECT * FROM gallery_photo WHERE id='$id'");
	return mysqli_fetch_assoc($result);
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Получение списка файлов галереи
// Функция возвращает список фотографий
function gallery_list($str, $direction)
{	
	// Подключение к БД.
	$db = my_connect_db();
	$arr = array();

//	 echo '<br> $path_big ='. $path_big;
//	 echo '<br> $path_small ='. $path_small;
        if ($direction != "asc"){
            $direction = "desc";
        }

        if ($str == "date") {
            $str = "id";
        } else {
            $str = "popul";
        }

        $query = "SELECT * "
                . "FROM gallery_photo "
                . "ORDER BY " .  
                $str . " " . $direction;
        
        $result = mysqli_query($db, $query);
        
//        echo $query;
        
//	if($str === 'date'){
//		$result = mysqli_query($db, "SELECT * FROM gallery_photo ORDER BY id ASC");
//        } else {	// popul is default
//		$result = mysqli_query($db, "SELECT * FROM gallery_photo ORDER BY popul DESC, id ASC");
//        }
        
        while($row = mysqli_fetch_assoc($result)){
		$arr[] = $row;
	}
		
	return $arr;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Возвращает путь к уменьшенному изображению
function gallery_icon($image, $path_small)
{	
//	 include('settings_path.php');
// 	echo '<br> $path_big ='. $path_big;
// 	echo '<br> $path_small ='. $path_small;
// 	echo '<br> path=='.$path_small . $image['name'] . '.' . $image['type'];
	return $path_small . $image['name'] . '.' . $image['type'];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Возвращает путь к полноразмерному изображению
function gallery_image($image, $path_big)
{	
//	 include('settings_path.php');
// 	echo '<br> \$path_big ='. $path_big;
	return $path_big.$image['name'] . '.' . $image['type'];
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Обновить популярность посещения фото
function gallery_update_popul($id)
{	
	// Подключение к БД.
	$db = my_connect_db();
	$arr = array();
	
	$id = (int)$id;
	$sql = "UPDATE gallery_photo SET popul=popul+1 WHERE id='$id'";
	return mysqli_query($db, $sql);
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Проверка доступности фото с $id
function gallery_access($id)
{	
	// Подключение к БД.
	$db = my_connect_db();
	
	$id = (int)$id;
	$sql = "SELECT id FROM gallery_photo WHERE id='$id'";
	if(mysqli_query($db, $sql) === false) {
		return false;
	} else {
		return true;
	}
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Проверка есть ли хоть одно изображение в галерее :)
function gallery_is_not_empty()
{
	// Подключение к БД.
	$db = my_connect_db();
	
	$sql = "SELECT id FROM gallery_photo ORDER BY popul, id LIMIT 1";
//	$sql = "SELECT id FROM gallery_photo WHERE name='QW' LIMIT 1";  //  строка для тестирования проверки есть ли хоть одно фото
	if(mysqli_query($db, $sql) === false) {
		return false;
	} else {
		return true;
	}
}