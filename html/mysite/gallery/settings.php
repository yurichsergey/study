<?php 
////////////////////////////////////////////////////////////////
// процедуры для корректности отображения unicode при работе с сессиями
mb_language('uni'); 
mb_internal_encoding('UTF-8');			

////////////////////////////////////////////////////////////////
// Часовой пояс
//date_default_timezone_set ( 'America/New_York' ); 
date_default_timezone_set ( 'Europe/Moscow' ); 


////////////////////////////////////////////////////////////////
// Путь для сохранения фотографий
$path_big = '../assets/gallery/big/';
$path_small = '../assets/gallery/small/';

