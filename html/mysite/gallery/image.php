<?php 
////////////////////////////////////////////////////////////////
// Настройки php, для работы с unicode и прочего...
require('settings.php');

////////////////////////////////////////////////////////////////
// Подключение модели
require('model/gallery.php');

////////////////////////////////////////////////////////////////
//
// ТОЧКА ВХОДА (Начало php-скрипта)
//
////////////////////////////////////////////////////////////////

session_start();

// Проверка есть ли хоть одно фото
if (gallery_is_not_empty() === false) {
    header("Location:index.php");
    exit();
}

// Если был GET запрос
if(isset($_GET['id']))
    $id = (int)$_GET['id'];

// Проверим доступно ли фото, если нет, то сбросим сессию
if(!gallery_access($id)){
    unset($_GET['id']);		// Сброс сессии
    unset($id);
}

// Если запрос пустой или если фото с таким id не существует
// удалить список популярнорсти фотографий
if(!isset($_GET['id']))
    unset($_SESSION['list_id']); 

// Получение списка файлов
if(!isset($_SESSION['list_id']))
{
    $_SESSION['sort'] = 'popul';
    $list = gallery_list($_SESSION['sort']);
    foreach($list as $i => $image)
        $_SESSION['list_id'][$i] = $image['id']; // запись порядка файлов по популярности в массив
    if(!isset($id))    // Если $id недоступна
        $id = $_SESSION['list_id'][0];
}

// Обновить популярность посещения фото
gallery_update_popul($id);

// вычисление id предыдущего и следующего фото
// $_SESSION['list_id'] - массив id фото, построенный по популярности
//
$n = count($_SESSION['list_id']);  // количество файлов в галерее
$i_id = array_search($id, $_SESSION['list_id']);  // Положение фото id в массиве $_SESSION['list_id']

if($i_id === 0){
    $prev = $_SESSION['list_id'][$n - 1];
    $next = $_SESSION['list_id'][$i_id + 1];
} elseif($i_id === ($n - 1)) {
    $prev = $_SESSION['list_id'][$i_id - 1];
    $next = $_SESSION['list_id'][0];
} else {
    $prev = $_SESSION['list_id'][$i_id - 1];
    $next = $_SESSION['list_id'][$i_id + 1];
}

// Подключение представления
$title = 'Просмотр фотографий';
$content = 'themplates/content_image.php';
require('themplates/main.php');