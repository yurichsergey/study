<?php
mb_language('uni'); 
mb_internal_encoding('UTF-8');			
// Часовой пояс
//date_default_timezone_set ( 'America/New_York' ); 
date_default_timezone_set ( 'Europe/Moscow' ); 

include_once('controller/Controller.php');
include_once('model/Database.php');
include_once('model/Task.php');
include_once('model/TaskFs.php');
include_once('model/TaskDb.php');
include_once('model/Log.php');
include_once('model/LogFs.php');
include_once('model/LogDb.php');

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// ТОЧКА ВХОДА (Начало php-скрипта)
//
////////////////////////////////////////////////////////////////


// Подключение к БД
//$db = new Database('mysql.sitescopy.ru', 'u352980549_root', 'cUra1bV7q5nic', 'u352980549_test');
$db = new Database('localhost', 'root', '', 'test');

//$Task = new TaskFs('task.txt');
// 	$task = new TaskDb($db);

// Создадим логгер
//$log = new LogFs('log.txt');
$log = new LogDb($db);

// Передача управления контроллеру
$controllerAuth = new Controller($db, $log);
$controllerAuth->Request();
