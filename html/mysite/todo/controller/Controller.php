<?php
class Controller
{
	private $_db;
	private $_log;
	private $_task;

	public function __construct($db, $log)
	{
		session_start();

		$this->_db = $db;
		$this->_log = $log;
		// Создадим модель (Экземпляр класса)
		//$task = new TaskFs('task.txt');
		$this->_task = new TaskDb($this->_db);
	}
	
	public function Request()
	{
		// Обработка POST.
		if (isset($_POST['task'])) 
		{   /* Была отправка формы (POST) */
			$task = trim($_POST['task']);
			$this->_task->Add($task);
			header('Location: ' . $_SERVER['PHP_SELF']);
			$this->_log->Add('Новая задача');
			exit();
		} else if (isset($_POST['id_task'])) {
			$id_task = $_POST['id_task'];
			$this->_task->Finish($id_task);
			header('Location: ' . $_SERVER['PHP_SELF']);
			$this->_log->Add('Задача выполнена');
			exit();
		}


		// Получение списка задач
		$tasks[] = new TaskDb($db);
		$tasks = $this->_task->GetAll();
		$list = $tasks;
		//exit();
		
		//$list = $this->_task->GetAll();

		
		// Выбор оформления
		function choise_view()
		{
			if(isset($_GET['theme']))
			{
				$theme = (int)$_GET['theme'];
				if($theme === 1 or $theme === 2)
				{
					$_SESSION['theme'] = $theme;
					$_COOKIE['theme'] = $theme;
					return $theme;
				}
			}
			if(isset($_SESSION['theme']))
				return (int)$_SESSION['theme'];
			if(isset($_COOKIE['theme']))
				return (int)$_COOKIE['theme'];
			return 1;
		}

		$theme = choise_view();
		$this->_log->Add('Список показан');

		// Подключение представления страницы
		if($theme === 1)
			include('view/view_1.php');
		elseif($theme === 2)
			include('view/view_2.php');
	}
}