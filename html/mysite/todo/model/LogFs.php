<?php
class LogFs extends Log
{
	private $_filename;
	
	public function __construct($filename)
	{
		$this->_filename = $filename;
	}
	
	protected function Save($msg, $ip, $time)
	{
		$f = fopen($this->_filename, 'a+');
		fwrite($f, "$time | $ip | $msg \n");
		fclose($f);
		parent::Save($msg, $ip, $time);
	}
}