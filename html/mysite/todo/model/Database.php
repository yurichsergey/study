<?
class Database
{
    private $_linkIdentifier;
    
    public function __construct($server, $user, $password, $dbName)
    {
        $this->_linkIdentifier = mysqli_connect($server, $user, $password, $dbName);
        mysqli_set_charset($this->_linkIdentifier, "utf8");

    }

    // ������������� ����. ��������
    public function EscapeString($text)
    {
        return mysqli_real_escape_string($this->_linkIdentifier, $text);
    }
    
    // ������ 
    public function Query($sql)
    {
        $result = mysqli_query($this->_linkIdentifier, $sql);
        $arr = array();
        
        while($row = mysqli_fetch_assoc($result))
            $arr[] = $row;
            
        return $arr;
    }

    public function Execute($sql)
    {
        mysqli_query($this->_linkIdentifier, $sql);
    }
}