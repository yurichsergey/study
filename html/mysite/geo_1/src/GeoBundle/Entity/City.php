<?php
/**
 * Description of City
 *
 * @author Sergey
 */
class City {
    protected $id;
    protected $country;
    protected $name;
    protected $region;
    protected $latitude;
    protected $longitude;
    
    function getId() {
        return $this->id;
    }

    function getCountry() {
        return $this->country;
    }

    function getName() {
        return $this->name;
    }

    function getRegion() {
        return $this->region;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }
    
    function setCountry($country) {
        $this->country = $country;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setRegion($region) {
        $this->region = $region;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }


}
