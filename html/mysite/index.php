﻿<?php   
//date_default_timezone_set ( 'America/New_York' ); 
date_default_timezone_set ( 'Europe/Moscow' ); 
// Запись в файл информации о посещении страницы
$f = fopen('visits.txt', 'a+');
fwrite($f, date('Y-m-d H:i:s') . "\n");
fwrite($f, $_SERVER['REMOTE_ADDR'] . "\n");
fwrite($f, $_SERVER['HTTP_REFERER'] . "\n");
fclose($f);
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
		<title>Страница web-работ Юрича Сергея</title>
		<style>
			<?php require 'style.css';?>
		</style>
		
	</head>
	<body>
					<h1>Страница web-работ Юрича Сергея</h1>

                    <ul>
                        <li><a href="gallery">Галерея фотографий</a></li>
                        <li><a href="mvc">Простейшая CMS</a></li>
                        <li><a href="exam">Аутентификация и тестирование</a></li>
                    </ul>

                    </br>	
		<div class="footer">
			<div class="footer-info">
				<hr/>
				<?php
					echo "<i><b>Current time (Текущее время): " . date('H:i:s') . "</b></i><br/>";
					echo "<i><b>Your IP adress (Ваш IP адресс): " . $_SERVER['REMOTE_ADDR'] . "</b></i><br/>";
					//echo "<i><a href=\"visits.php\"><b>Статистика посещений</b></a></i>";
				?>
			</div>
		</div>
	</body>
</html>