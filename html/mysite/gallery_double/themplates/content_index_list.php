<center><h3>
<a href="/index.php">Главная</a> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
<a href="/all_ex.php">Все упражнения</a></h3>
<h1><i><u>Галерея фотографий</u></i></h1>
</center>

<table align="center" width="300"> 
    <tr> 
        <td valign="top" align="left">
            <b>Отобразить <br/><a href="<?=$_SERVER['PHP_SELF']?>?view=table">таблицей</a></b>
        </td>
        <td valign="top" align="right"><b>
            <?if($_SESSION['sort'] === 'date'):?>
                Сортировать по <br/><a href="<?=$_SERVER['PHP_SELF']?>?sort=popul">популярности</a>
            <?else:?>
                Сортировать по <br/><a href="<?=$_SERVER['PHP_SELF']?>?sort=date">дате добавления</a>
            <?endif?>
        </b></td>
    </tr>
    <tr> 
        <td colspan="2" valign="top" align="left"><h3>
            <?if($_SESSION['sort'] === 'date'):?>
                Отсортировано по дате добавления
            <?else:?>
                Отсортировано по популярности
            <?endif?>
        </h3></td>
    </tr>
</table>
<table align="center" width="300" border="2"> 
    <tr> 
        <td colspan="2" valign="top" align="left">
            <form method="post" enctype="multipart/form-data">
                <h3>Добавить изображение<small>*</small>: &nbsp &nbsp
                <input type="file" name="fname" > 
                <input type="submit" value="Загрузить изображение!" /></h3>
            </form>
        </td>
    </tr>
</table>
<table align="center" width="300"> 
    <tr> 
        <td valign="top" align="left">
            <?/// Информационное сообщение / Была отправка формы (POST) /
            // if (isset($_GET['message'])) {   echo $_GET['message'];} 
            // $_SESSION['message'][0] - Основное информационное сообщение (ошибка, успех...)
            // $_SESSION['message'][1] - Примечание к сообщению   ?>
            <? if (isset($_SESSION['message'][0])): ?>
                <br/><center><h2><?=$_SESSION['message'][0]?></h2></center><br/>
                <?unset($_SESSION['message'][0])?>
            <?endif?>
            <? if (isset($_SESSION['message'][1])): ?>
                <? if ($_SESSION['message'][1] !== ''): ?>
                    <center><u><big><?=$_SESSION['message'][1]?></big></u><br/><br/><br/></center>
                    <?unset($_SESSION['message'][1])?>
                <?endif?>
            <?endif?>
        </td>
    </tr>
</table>
<?// Печать галереи фото?>
<?if(count($list) !== 0):?>
    <table align="center" width="300" cellpadding="8" border="1" bordercolor="silver"><tr>
    <?foreach($list as $i => $image):?>
        <td align="center" valign="center">
            <a href="image.php?id=<?=$image['id']?>">
                <img src="<?=$modelGallery->Icon($image)?>" alt="<?=$image['id']?>"/>
            </a>
        </td>
            </tr><tr>
    <?endforeach?>
    </tr></table>
<?else:?>
    <center><h1>Галерея пуста. Добавьте свои фотографии.</h1></center>
<?endif?>

<br/><br/><br/><center><a href="<?=$_SERVER['PHP_SELF']?>"><h3>Вернуться к галерее фотографий</h3></a></center>
<table align="center" width="300"> 
    <tr> 
        <td valign="top" align="left">
            _____________________________________<br/>
            <small>*Пожалуста, загружайте только изображения следующих форматов: GIF, PNG, JPEG, WBMP, XBM.</small><br/>
            <small> &nbsp Размер файла не должен превышать <b><?=$post_max_size?></b>.</small><br/>
        </td>
    </tr>
</table>

