<table align="center" width="980" > 
    <tr> <!---Заголовок-->
        <td align="left"><a href="/index.php"><h2>Главная страница</h2></a></td>
        <td align="center"><h1><i><u>Галерея фотографий</u></i></h1></td>
        <td align="right"><a href="/all_ex.php"><h2>Все упражнения</h2></a></td></tr>
    </tr>
</table>
<table align="center" width="980" border="2"> 
    <tr> 
        <td valign="center" align="center">
            <h2>Отобразить <a href="<?=$_SERVER['PHP_SELF']?>?view=table">таблицей</a> или 
                <a href="<?=$_SERVER['PHP_SELF']?>?view=list">списком</h2>
        </td>
        <td valign="top" align="center">
            <h2>Сортировать по <a href="<?=$_SERVER['PHP_SELF']?>?sort=popul">популярности</a> или 
                <a href="<?=$_SERVER['PHP_SELF']?>?sort=date">дате добавления</a></h2>
            <h2>
            <?if($_SESSION['sort'] === 'date'):?>
                Отсортировано по дате добавления
            <?else:?>
                Отсортировано по популярности
            <?endif?>
            </h2>                
        </td>
    </tr>
    <tr> 
        <td colspan="2" valign="top" align="center">
            <form method="post" enctype="multipart/form-data">
                <h3>Добавить изображение<small>*</small> в галерею: &nbsp &nbsp
                <input type="file" name="fname" > &nbsp &nbsp &nbsp &nbsp 
                <input type="submit" value="Загрузить изображение!" /></h3>
            </form>
        </td>
    </tr>
</table>
<?/// Информационное сообщение / Была отправка формы (POST) /
// if (isset($_GET['message'])) {   echo $_GET['message'];} 
// $_SESSION['message'][0] - Основное информационное сообщение (ошибка, успех...)
// $_SESSION['message'][1] - Примечание к сообщению   ?>
<? if (isset($_SESSION['message'][0])): ?>
    <br/><center><h1><?=$_SESSION['message'][0]?></h1></center><br/>
    <?unset($_SESSION['message'][0])?>
<?endif?>
<? if (isset($_SESSION['message'][1])): ?>
    <? if ($_SESSION['message'][1] !== ''): ?>
        <center><u><big><?=$_SESSION['message'][1]?></big></u><br/><br/><br/></center>
        <?unset($_SESSION['message'][1])?>
    <?endif?>
<?endif?>
<?// Печать галереи фото?>
<?if(count($list) !== 0):?>
    <table align="center" width="980" cellpadding="8" border="1" bordercolor="silver"><tr>
    <?foreach($list as $i => $image):?>
        <td align="center" valign="center">
            <a href="image.php?id=<?=$image['id']?>">
                <img src="<?=$modelGallery->Icon($image)?>" alt="<?=$image['id']?>"/>
            </a>
        </td>
        <?if(($i+1) % 5 === 0):?>
            </tr><tr>
        <?endif?> 
    <?endforeach?>
    </tr></table>
<?else:?>
    <center><h1>Галерея пуста. Добавьте свои фотографии.</h1></center>
<?endif?>

<br/><br/><br/><center><a href="<?=$_SERVER['PHP_SELF']?>"><h3>Вернуться к галерее фотографий</h3></a></center>
<table align="center" width="960"> 
    <tr> 
        <td valign="top" align="left">
            _________________________________________________________________________________________________________________________<br/>
            <small>*Пожалуста, загружайте только изображения следующих форматов: GIF, PNG, JPEG, WBMP, XBM.</small><br/>
            <small> &nbsp Размер файла не должен превышать <b><?=$post_max_size?></b>.</small><br/>
        </td>
    </tr>
</table>

