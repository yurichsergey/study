<?php 
// Настройки php, для работы с unicode и прочего...
require_once('settings_php.php');

// Подключение драйвера базы данных
require_once('model/Database.php');

// Подключение модели галереи изображений
require_once('model/ModelGallery.php');

// Функция определения максимально размера загружаемого файла на сервер по метоеду POST
function return_post_max_size() 
{
    $val = ini_get('post_max_size');
    $val = trim($val);
    $val = mb_convert_case($val, MB_CASE_LOWER);
    $last = mb_substr($val,mb_strlen($val)-1,1);
    $val = trim( mb_substr($val,0,mb_strlen($val)-1) ); /// Оставим только число
    switch($last) {
        // Модификатор 'G' доступен, начиная с PHP 5.1.0
        case 'g':   return $val . ' ГБ';
        case 'm':   return $val . ' МБ';
        case 'k':   return $val . ' КБ';
        default:    return $val . ' Б';
    }
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// ТОЧКА ВХОДА (Начало php-скрипта)
//
////////////////////////////////////////////////////////////////

session_start();

$db = new Database('localhost', 'root', '', 'test');
$modelGallery = new ModelGallery($db);

// Пути к хранилищу изображений
include('settings_path.php');

// Проверка существования каталогов для хранения файлов
if (!file_exists($path_big))     mkdir($path_big, 0755);
if (!file_exists($path_small))   mkdir($path_small, 0755);

// Проверка, был ли POST запрос, если был, то загрузить фото
if (isset($_FILES['fname'])) 
{
    $message = $modelGallery->Add($_FILES['fname']['tmp_name'], $_FILES['fname']['name']);
    $_SESSION['message'][0] = $message[1];  // $message[1] - Основное информационное сообщение (ошибка, успех...)
    $_SESSION['message'][1] = $message[2];  // $message[2] - Примечание к сообщению

    // Перезагрузим страницу
    header("Location: ". $_SERVER['PHP_SELF']);
    exit();
}

// максимальный размер загружаемого файла на сервер по методу POST (тип - string)
$post_max_size = return_post_max_size();

// Получение списка файлов
if($_GET['sort'] === 'date')
    $_SESSION['sort'] = 'date';
elseif($_GET['sort'] === 'popul')
    $_SESSION['sort'] = 'popul';
elseif(!isset($_SESSION['sort']))
    $_SESSION['sort'] = 'popul';

//
if($_SESSION['sort'] === 'date')
    $list = $modelGallery->ListImages('date');
elseif($_SESSION['sort'] === 'popul')
    $list = $modelGallery->ListImages('popul');

// 
unset($_SESSION['list_id']);
foreach($list as $i => $image)
    $_SESSION['list_id'][$i] = $image['id']; // запись порядка файлов в массив

// Выбор отображения: таблица или список
if($_GET['view'] === 'list')
    $_SESSION['view'] = 'list';
elseif($_GET['view'] === 'table')
    $_SESSION['view'] = 'table';
elseif(!isset($_SESSION['view']))
    $_SESSION['view'] = 'table';

// Подключение пути с отображением    
if($_SESSION['view'] === 'list')
    $content = 'themplates/content_index_list.php';
elseif($_SESSION['view'] === 'table')
    $content = 'themplates/content_index_table.php';

// Подключение представления
$title = 'Галерея фотографий';
require_once('themplates/main.php');