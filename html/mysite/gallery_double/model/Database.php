<?
class Database
{
    private $_linkIdentifier;
    
    public function __construct($server, $user, $password, $dbName)
    {
        $this->_linkIdentifier = mysqli_connect($server, $user, $password, $dbName);
        mysqli_set_charset($this->_linkIdentifier, "utf8");
    }

    // Экранирование спец. символов
    public function EscapeString($text)
    {
        return mysqli_real_escape_string($this->_linkIdentifier, $text);
    }
    
    // Запрос, возвращает массив в случае успешного выполнения запроса SELECT (и, возможно, SHOW, DESCRIBE или EXPLAIN)
    // True - в случае успешного выполнения запросов UPDATE, INSERT и других запросах (всех кроме SELECT, SHOW, DESCRIBE или EXPLAIN)
    // False - неудача
    public function Query($sql)
    {
        //echo '<br/><br/>'.$sql.'<br/>';
        $result = mysqli_query($this->_linkIdentifier, $sql);
        if($result !== false and $result !== true)
        {
            $arr = array();
            while($row = mysqli_fetch_assoc($result))
                $arr[] = $row;
/*  
            foreach($arr as $key => $val)
            {
                echo '<br/>key = '.$key.' | | | | | val = '.$val.'<br/>';
                foreach($val as $k => $v)
                {
                    echo 'k = '.$k.' | | | | | | | v = '.$v.'<br/>';
                }
            }
            echo 'row = '.$arr[0]['id'].' &nbsp&nbsp&nbsp count(arr) = '.count($arr,1).' &nbsp&nbsp&nbsp count(row) = '.count($row,1).'<br/>';
*/

            // Велосипед !!!! :-) ХА-ХА-ХА
            if(count($arr,0) === 1)
                return $arr[0];
                
            return $arr;
        }
        elseif($result === true)
        {
            //echo 'true <br/>';
            return true;
        }
        elseif($result === false)
        {
            //echo 'false <br/>';
            return false;
        }
    }
}