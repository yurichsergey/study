<?php 
class ModelGallery
{
	private $_db;
	
    // Подключение к БД
    public function __construct($db)
    {
		$this->_db = $db;
    }


    ////////////////////////////////////////////////////////////////
    // 
    // image_resize - Функция сохранения копии изображения
    //
    // Функция уменьшения изображения и сохранения его на диске
    // (Используется при добавлении фото в галерею)
    // 
    // Функция уменьшает изображения так, чтобы максимальный размер 
    // нового изображения по горизонтали или вертикали не превышал 
    // $MaxPix  и сохраняет в $file_path_new
    // 
    // $file_path_new - путь к сохраняемому изображению
    // $file_path - путь к исходному изображению
    // $imagewidth - ширины исходного изображения
    // $imageheigth - высота исоходного изображения
    // $imagetype - тип изображения
    // $MaxPix - максимальный размер нового изображения по горизонтали или вертикали
    //
    // image_resize() ничего не возвращает
    //
    // В image_resize() не используется getimagesize() для оптимизации,
    // т.к. эта информация доступна из функции в которой она вызывается 
    //
    private function ImageResize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, $MaxPix)
    {
        // Уменьшение размера изображения до $MaxPix по максимальной стороне
        $image_src = call_user_func('imagecreatefrom' . $imagetype, $file_path);
        if($imagewidth <= $MaxPix && $imageheigth <= $MaxPix)
        {
            $new_heigth = $imageheigth;
            $new_width = $imagewidth;
        }
        else
        {
            if($imagewidth > $imageheigth)
            {
                $new_heigth = ($imageheigth * $MaxPix / $imagewidth);
                $new_width = $MaxPix;
            }
            else
            {
                $new_heigth = $MaxPix;
                $new_width = ($imagewidth * $MaxPix / $imageheigth);
            }
        }
        $image_dst = imagecreatetruecolor($new_width, $new_heigth);
        imagecopyresampled($image_dst, $image_src, 0, 0, 0, 0, $new_width, $new_heigth, $imagewidth, $imageheigth);

        switch($imagetype):      // IMAGETYPE_XXX
            case "jpeg":
                imagejpeg($image_dst, $file_path_new, 90);
                break;
            default:
                call_user_func('image' . $imagetype, $image_dst, $file_path_new);
                break;
        endswitch;
    }


    ////////////////////////////////////////////////////////////////
    //
    // Функция обработки загруженного изображения
    // Добавляет файл в галерею
    //
    // Тип файла определяется из свойств самого файла
    //
    // $message[0] - true or false
    // $message[1] - Основное информационное сообщение (ошибка, успех...)
    // $message[2] - Примечание к сообщению
    // 
    //  Возвращает в случае успеха:  true
    //  
    public function Add($file_path, $file_name)
    {    
        // Проверим, пустой ли запрос
        if ($file_name === '' or $file_path === ''){
            $message[0] = false;
            $message[1] = 'Вначале выберите файл!';
            $message[2] = '';
            return $message; }

        // Проверка типа файла
        // Тип файла определяется из свойств самого файла
        $imageinfo = getimagesize($file_path);
        $imagewidth = $imageinfo[0];    // ширина
        $imageheigth = $imageinfo[1];   // высота
        $imagetype = $imageinfo[2];
        switch($imageinfo[2]):      // IMAGETYPE_XXX
            case IMAGETYPE_PNG:     $imagetype = "png";     break;
            case IMAGETYPE_GIF:     $imagetype = "gif";     break;
            case IMAGETYPE_JPEG:    $imagetype = "jpeg";    break;
            case IMAGETYPE_JPEG2000: $imagetype = "jpeg";   break;  // ???? 
            case IMAGETYPE_WBMP:    $imagetype = "wbmp";    break;
            case IMAGETYPE_XBM:     $imagetype = "xbm";     break;
            default:
                $message[0] = false;
                $message[1] = 'Файл "' . $file_name . '" не загружен! Неизвестный тип файла!';
                $message[2] = 'Пожалуста, загружайте только изображения следующих форматов: GIF, PNG, JPEG, WBMP, XBM!';
                return $message; 
        endswitch;

        // Идентификатор фото в хранилище (его создание)
        $newname = microtime(true); // Время в микросекундах :)
        $newname = $newname - (int)$newname;
        $dateimage = date('Y M d D H-i-s') . sprintf("-%f",$newname);
        $newname = $dateimage . ' ' . $file_name;    //    echo $newname . '<br/>';
        $newname = md5($newname);
		// while(file_exists($newname)) {   $numberfiles++;   $newname = './big/'. $numberfiles .'.jpg';  }

        // Добавляет информацию об изображении в базу данных
		$this->_db->Query("INSERT INTO photo (id, name, type, popul, time) 
                            VALUES (NULL, '$newname', '$imagetype', '0', '$dateimage')");
            
        // Пути к хранилищу изображений
        include('settings_path.php');

        // Сохранение файла в хранилище больших снимков и создание эскиза
        $file_path_new = $path_big.$newname.'.'.$imagetype;
        ImageResize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, 960);
        $file_path_new = $path_small.$newname.'.'.$imagetype;
        ImageResize($file_path_new, $file_path, $imagewidth, $imageheigth, $imagetype, 175);
        
        // отчет об успехе добавления файла в галерею
        $message[0] = true;
        $message[1] = 'Файл "' . $file_name . '" успешно загружен!';
        $message[2] = '';
        return $message;
    }

    // Функция возвращает объект фотографии (ассоциативный массив)
    public function Item($id)
    {   
        $id = (int)$id;
        return $this->_db->Query("SELECT * FROM photo WHERE id='$id'");
    }


    // Получение списка файлов галереи
    // Функция возвращает список фотографий
    public function ListImages($str)
    {    
        if($str === 'date')
            return $this->_db->Query("SELECT * FROM photo ORDER BY id ASC");
        else    // popul is default
            return $this->_db->Query("SELECT * FROM photo ORDER BY popul DESC, id ASC");
    }

    // Возвращает путь к уменьшенному изображению
    public function Icon($image)
    {    
        // Пути к хранилищу изображений
        include('settings_path.php');
        return $path_small . $image['name'] . '.' . $image['type'];
    }


    // Возвращает путь к полноразмерному изображению
    public function Image($image)
    {    
        // Пути к хранилищу изображений
        include('settings_path.php');
        return $path_big.$image['name'] . '.' . $image['type'];
    }


    // Обновить популярность посещения фото
    public function UpdatePopul($id)
    {    
        $id = (int)$id;
        return $this->_db->Query("UPDATE photo SET popul=popul+1 WHERE id='$id'");
    }


    // Проверка доступности фото с $id
    public function Access($id)
    {   
        $id = (int)$id;
        return $this->_db->Query("SELECT id FROM photo WHERE id='$id'");
    }


    // Проверка есть ли хоть одно изображение в галерее :)
    public function IsNotEmpty()
    {   
        return $this->_db->Query("SELECT id FROM photo ORDER BY id LIMIT 1");
    }

}