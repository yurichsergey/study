<?php 
// Настройки php, для работы с unicode и прочего...
require_once('settings_php.php');

// Подключение драйвера базы данных
require_once('model/Database.php');

// Подключение модели галереи изображений
require_once('model/ModelGallery.php');

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//
// ТОЧКА ВХОДА (Начало php-скрипта)
//
////////////////////////////////////////////////////////////////

session_start();

$db = new Database('localhost', 'root', '', 'test');
$modelGallery = new ModelGallery($db);

// Проверка есть ли хоть одно фото
if ($modelGallery->IsNotEmpty() === false) {
    header("Location:index.php");
    exit();
}

// Если был GET запрос
if(isset($_GET['id']))
    $id = (int)$_GET['id'];

// Проверим доступно ли фото, если нет, то сбросим сессию
if(!$modelGallery->Access($id)){
    unset($_GET['id']);		// Сброс сессии
    unset($id);
}

// Если запрос пустой или если фото с таким id не существует
// удалить список популярнорсти фотографий
if(!isset($_GET['id']))
    unset($_SESSION['list_id']); 

// Получение списка файлов
if(!isset($_SESSION['list_id']))
{
    $_SESSION['sort'] = 'popul';
    $list = $modelGallery->ListImages($_SESSION['sort']);
    foreach($list as $i => $image)
        $_SESSION['list_id'][$i] = $image['id']; // запись порядка файлов в массив
    if(!isset($id))    // Если $id недоступна
        $id = $_SESSION['list_id'][0];
}

// Обновить популярность посещения фото
$modelGallery->UpdatePopul($id);

// вычисление id предыдущего и следующего фото
// $_SESSION['list_id'] - массив id фото
$n = count($_SESSION['list_id']);  // количество файлов в галерее
$i_id = array_search($id, $_SESSION['list_id']);  // Положение фото id в массиве $_SESSION['list_id']

if($i_id === 0){
    $prev = $_SESSION['list_id'][$n - 1];
    $next = $_SESSION['list_id'][$i_id + 1];
} elseif($i_id === ($n - 1)) {
    $prev = $_SESSION['list_id'][$i_id - 1];
    $next = $_SESSION['list_id'][0];
} else {
    $prev = $_SESSION['list_id'][$i_id - 1];
    $next = $_SESSION['list_id'][$i_id + 1];
}

// Подключение представления
$title = 'Просмотр фотографий';
$content = 'themplates/content_image.php';
require_once('themplates/main.php');