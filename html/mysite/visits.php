﻿<?php
date_default_timezone_set ( 'Europe/Moscow' ); 
// Чтение из файла всей информации 
$lines = file('visits.txt');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
		<title>Журнал посещений</title>
	</head>
	<body>
        <table align="center" width="980" > 
            <tr> <!---Заголовок-->
                <td align="left"><a href="/index.php"><h2>Главная страница</h2></a></td>
                <td align="right"><a href="/all_ex.php"><h2>Все упражнения</h2></a></td></tr>
            </tr>
        </table>
        <h1><center><i>Журнал посещений главной страницы</i></center></h1>
		<table align="center" width="980" cellpadding="8" border="1">
			<tr>
				<td width="150">Время</td>
				<td>IP-адрес</td>
				<td>Откуда</td>
			</tr>
            <?php
            $n = count($lines);
            for($i = 0; $i < $n; $i+=3)
            {
                echo '<tr>';
                echo '<td>' . $lines[$i + 0]  . '</td>';
                echo '<td>' . $lines[$i + 1]  . '</td>';
                echo '<td>' . $lines[$i + 2]  . '</td>';
                echo '</tr>';
            }
            ?>
		</table>
        <br/><br/>
	</body>
</html>

