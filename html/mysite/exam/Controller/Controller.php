<?php
class Controller
{
	protected $db;
	protected $auth;
	protected $model;
	public $message;
	
	public function  __construct() 
	{
		$this->db = new Database('127.0.0.1:3306', 'root', '', 'helloworld_product');
		$this->auth = new Auth($this->db);
		$this->model = new Model($this->db, $this->auth);	
	}
	
	public function Start()
	{
		if(isset($_POST["update"])){
			header("Location: .");
			exit();
		}
		
		if ($this->Authentication())
		{

			if(isset($_POST['page_next']))
			{
				if (isset($_POST['answer_id']))
				{
					$this->model->SetUserAnswer();
				}
				else
				{
					$this->message .= "Пожалуйста, выберите вариант ответа: <br/>";
					unset($_POST['next']);
					$_SESSION['page_current'] = "page_exam";
				}
				header("Location: .");
								
			}
			
			if (!isset($_SESSION['list_id']))
			{
				$this->model->GetListQuestions();	
			}
			
			if ($this->model->GetUserTested())
				$_SESSION['page_current'] = 'page_result';

			
			if ($_SESSION['page_current'] == 'page_exam')
			{
				$this->model->GetQuestionCurrent();
				$this->model->GetQuestionNext();
				$this->model->GetAnswerCurrent();
				
				$title = "Тестирование";
				$content = "View/PageBase.php";
				$content2 = "View/PageExam.php";
				
				$question = $this->model->_question;
				$answers = $this->model->_answers;
				$answer_current = $this->model->_answer_current;
				$current = $this->model->_current;
				$next = $this->model->_next;
			}
			else if ($_SESSION['page_current'] == 'page_result')
			{
				$this->model->SetUserTested();
				$this->model->GetAllQuestionsAndAnswers();
				
				$title = "Результаты тестирования";
				$content = "View/PageBase.php";
				$content2 = "View/PageResult.php";
				
				$username = $this->auth->_firstname . " " . $this->auth->_lastname;
				$numberOfRightAnswers = $this->model->numberOfRightAnswers;
				$numberOfQuestions = $this->model->numberOfQuestions;
				$allResult = $this->model->allResult;
			}
				
		}
		else
		{
			if ($_SESSION['page_current'] == 'registration')
			{
				$title = "Регистрация на сайте";
				$content = "View/Reg.php";
			}
			else
			{
				$title = "Тестирование на знание русской истории";
				$content = "View/Login.php";
			}
		}
		
		$message = $this->auth->_message . $this->message;
		$login = $this->auth->_login;
		
		include 'View/Main.php';
	}
	
	
	
	protected function Authentication()
	{
		if (isset($_POST['logout']))
			if ($_POST['logout'] == "true")
			{
				$this->auth->Logout();
				header("Location: .");
				return false;
			}
	
		if ($this->auth->Verification())
		{
			if (isset($_POST['page_next']))
				$_SESSION['page_current'] = $_POST['page_next'];
			else if (isset($_COOKIES['page_current']))
				$_SESSION['page_current'] = $_COOKIES['page_current'];
			else if (!isset($_SESSION['page_current']))
				$_SESSION['page_current'] = 'page_exam';
				
			if (isset($_SESSION['remember']))
				if ($_SESSION['remember'])
					setcookie('page_current', $_SESSION['page_current'], time() + 3600 * 24 * 30);
				else if (isset($_COOKIE['remember']))
					if ($_COOKIE['remember'])
						$_SESSION['remember'] = true;
					
			return true;
		}
		else if (isset($_POST['registration']))
		{
			if ($_POST['registration'] == "true")
			{
				if ($this->auth->Registration())
				{
					if ($this->auth->Login($_POST['login'], $_POST['password'], $_POST['remember'] == "true"))
					{
						$_SESSION['page_current'] = 'page_exam';
						header("Location: .");
						return true;
					}
					else
					{
						$_SESSION['page_current'] = 'page_login';
						return false;
					}
				}
				else
				{
					$_SESSION['page_current'] = "registration";
					return false;
				}
			}
			else
			{
				$_SESSION['page_current'] = "registration";
				return false;
			}
		}
		else  // Login
		{
			//			$auth->Logout();
			$_SESSION['page_current'] = 'page_login';
	
			if (isset($_POST['page_login']))
				if ($_POST['page_login'] == "true")
				{
					if ($this->auth->Login($_POST['login'], $_POST['password'], $_POST['remember'] == "true"))
					{
						$_SESSION['page_current'] = 'page_exam';
						header("Location: .");
						return true;
					}
				}
			return false;
		}
	
	}
	
}