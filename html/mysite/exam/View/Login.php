<table cellpadding="20" width="1000">
	<tbody >
		<tr valign="top">
			<td>
				<h1> Вход на сайт</h1>
				<form action="" method="post">
					Введите логин:<br/>
					<input type="text" name="login" placeholder="логин" size="40"
							required pattern="^[A-Za-z][-0-9A-Za-z.@_]{0,63}$" /><br/><br/>
					Введите пароль:<br/>
					<input type="password" name="password" placeholder="пароль" size="40"
							required pattern="^[-0-9A-Za-z.@_]{8,128}$" /><br/><br/>
				<!-- 	<input type="checkbox" name="remember" value="true" /> Запоминать меня (на месяц)<br/><br/>  -->
					<input type="submit" value="Войти" />
					<input type="hidden" name="page_login" value="true"  />
				</form>
				<br/>
				<form action="" method="post">
					<input type="submit" value="Регистрация" />
					<input type="hidden" name="registration" value="false"  />
				</form>
			</td>
			<td >
				<br/>
				<br/>
				<br/>
				<br/>
				<p><big>Добро пожаловать на страницу по тестированию!</big></p>
				<p>Вы можете выбирать один из вариантов ответа. Пожалуйста, не торопитесь, 
					у вас не будет возможности вернуться к предыдущему вопросу. Перед началом тестирования необходимо зарегистрироваться.</p>
			</td>
		</tr>
	</tbody>
</table>