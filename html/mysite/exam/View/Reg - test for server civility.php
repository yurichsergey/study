
<form action="" method="post">
	<table>
		<tbody valign="top">
			<tr >
				<td>
					Введите свое имя*:<br/>
					<input type="text" name="firstname" placeholder="имя" maxlength="128" 
						 value="<?=$_SESSION['firstname']?>" 
						size="40"
						/><br/><br/>
							
					Введите свою фамилию:<br/>
					<input type="text" name="lastname" placeholder="фамилия" maxlength="128" 
						value="<?=$_SESSION['lastname']?>" 
						size="40"
						/><br/><br/>
				</td>
				<td>
					<font color="grey">
					При вводе имени и фамилии ипользуйте только <br/>
					английский и русский алфавит в нижнем и верхнем <br/>
					регистрах, цифры, пробел и знак дефиса ("<b>-</b>"). <br/>
					Имя и фамилия могут быть не длинее 128 символов.<br/><br/>
					</font>
				</td>
			</tr>
			<tr>
				<td>
					Введите логин*:<br/>
					<input type="text" name="username" placeholder="логин" maxlength="64" 
						 value="<?=$_SESSION['username']?>"
						size="40"
						/><br/><br/>
				</td>
				<td>
					<font color="grey">
					При вводе логина ипользуйте только английский <br/>
					алфавит в нижнем и верхнем регистрах, цифры и <br/>
					следующие знаки: "<b>.</b>" (точка), "<b>@</b>" (коммерческое at), <br/>
					"<b>_</b>" (нижнее подчеркивание), "<b>-</b>" (дефис). 
					Логин может <br/>начинаться только с английской буквы в любом регистре. <br/>
					Логин должен быть длиной от 1 до 64 символов.<br/><br/>
					</font>
				</td>
			</tr>
			<tr>
				<td>
					Введите пароль*:<br/>
					<input type="password" name="password" placeholder="пароль" maxlength="128" 
						
						size="40"
						/><br/><br/>
					
					Повторите пароль*:<br/>
					<input type="password" name="password2" placeholder="пароль" maxlength="128" 
						
						size="40"
						/><br/><br/>
<!--  					pattern="[0-9A-Za-z,.?!@#$%^&*()_=+;:'-]{8,128}" size="40"  -->						
					</td>
				<td>
					<font color="grey">
					При вводе пароля ипользуйте только английский <br/>
					алфавит в нижнем и верхнем регистрах, цифры и <br/>
					следующие знаки: "<b>.</b>" (точка), "<b>@</b>" (коммерческое at), <br/>
					"<b>_</b>" (нижнее подчеркивание), "<b>-</b>" (дефис). <br/> 
					В пароле должно быть от 8 и до 128 символов.
					</font>
				</td>
			</tr>
		</tbody>
	</table>	
	<input type="checkbox" name="remember" value="true" /> Запоминать меня (на месяц)<br/><br/>
	
	<input type="submit" value="Зарегистрироваться" />
	<input type="hidden" name="registration" value="true"  />
</form>
<br/>

<form action="" method="post">
	<input type="submit" value="Войти" />
	<input type="hidden" name="login" value="true"  />
</form>
<br/>
________________________________________________________________________<br/>
* Поля обязательные для заполнения
