<h3>Уважаемый, <BIG><?=$username ?></BIG>, вы ответили правильно на 
	<big><?=$numberOfRightAnswers ?></big> вопросов из <big><?=$numberOfQuestions ?></big>.</h3>
<br/>
<table width="1000" border="2" cellpadding="8">
	<thead bgcolor="grey"> 
		<tr>
			<th>
				<big>Вопрос</big>
			</th>
			<th>
				<big>Правильный ответ</big>
			</th>
			<th>
				<big>Ваш ответ</big>
			</th>
		</tr> 
	</thead>
	<tbody>
		<?php foreach ($allResult as $k => $question):?>
		<tr bgcolor="<?php if($allResult[$k]['right']):?> 
						<?="green"?> 
					<?php else :?>
						<?= "red"?>
					<?php endif;?>">
			<td>
				<?=$allResult[$k]['question']?>
			</td>
			<td>
				<?=$allResult[$k]['answer_right']?>
			</td>
			<td>
				<?=$allResult[$k]['answer_user']?>
			</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>


<br/>
<br/>
