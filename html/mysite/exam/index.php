<?php
	////////////////////////////////////////////////////////////////
	// процедуры для корректности отображения unicode при работе с сессиями
	mb_language('uni');
	mb_internal_encoding('UTF-8');
	//date_default_timezone_set ( 'America/New_York' );
	date_default_timezone_set ( 'Europe/Moscow' );

	include 'Controller/Controller.php';
	include 'Model/Auth.php';
	include 'Model/Model.php';
	include 'Model/Database.php';
	

	$controller = new Controller();
	$controller->Start();