<?php
class Model 
{
	
	public $_question;
	public $_answers;
	public $_answer_current; 
	public $_current;
	public $_next;
	public $_prev;
	
	public $questions;
	public $answers;
	public $user_answers;
	public $allResult;
	public $numberOfRightAnswers;
	public $numberOfWrongAnswers;
	public $numberOfQuestions;
	
	protected $_db;
	protected $_auth;
	
	public function __construct($db, $auth)
	{
		$this->_db = $db;
		$this->_auth = $auth;

	}
	
	
		
	public function GetListQuestions()
	{
// 		if(isset($_COOKIE['list_id']))
// 		{
// 			$_SESSION['list_id'] = $_COOKIE['list_id'];
// 		}
// 		else
// 		{
			$list = $this->_db->Query("SELECT id FROM exam_questions ORDER BY id ASC");
				
			foreach($list as $i => $question)
			{
				$_SESSION['list_id'][$i] = $question['id'];
			}
				
// 			if($_SESSION['remember'])
// 				$_COOKIE['list_id'] = $_SESSION['list_id'];
// 		}
		
		return true;
	}
		
	
	public function GetQuestionCurrent()
	{
		if(isset($_POST['next']))
		{
			$_SESSION['current'] = $_POST['next'];
		}
// 		else if(!isset($_SESSION['current']) & isset($_COOKIE['current']))
// 		{
// 			$_SESSION['current'] = $_COOKIE['current'];
// 		}
		else if(!isset($_SESSION['current']))
		{
			$_SESSION['current'] = $_SESSION['list_id'][0];

// 			if($_SESSION['remember'])
// 				$_COOKIE['current'] = $_SESSION['current'];
		}

// 		echo "<br/>current = ".$_SESSION['current'];
// 		echo "<br/>next = ".$_POST['next'];
		
//		$numberQuestions = count($_SESSION['list_id']);
		
		$this->_current = $_SESSION['current'];

		$this->_current = (int)$this->_current;
		$arr = $this->_db->Query("SELECT exam_answers.id, exam_answers.answer, exam_questions.question 
							FROM exam_questions, exam_answers
							WHERE exam_questions.id=exam_answers.question_id 
							AND exam_answers.question_id='$this->_current'");
		
		$this->_question = $arr[0]["question"];
		$i = 0;
		foreach ($arr as $question_id => $answer_all)
		{
			$this->_answers[][0] = $arr[$i++]["id"]; // answer ID
			foreach ($answer_all as $column_name => $value)
			{
				if($column_name == "answer")
				{
					$this->_answers[][1] = $value;
				}
			}		
		}
		return true;
	}	


	public function GetQuestionNext() 
	{
		$this->_next = $_SESSION['list_id'][$_SESSION['current'] ];
		if($this->_next == $numberQuestions)
			$this->_next = -1;
	}
	
	public function GetAnswerCurrent()
	{
		$this->_current = (int)$this->_current;
		$user_id = (int)$this->_auth->_userID;
		$arr = $this->_db->Query("SELECT answer_id FROM exam_user_answers
				WHERE question_id='$this->_current' AND user_id='$user_id'");
		if ($arr != false) {
                    $answer_id = $arr[0]['answer_id'];
                }

        $this->_answer_current = $answer_id;
	}
	

	public function GetUserTested()
	{
		$user_id = (int)$this->_auth->_userID;
		$arr = $this->_db->Query("SELECT tested FROM exam_users WHERE id='$user_id'");
		if($arr != false)
			$this->_tested = $arr[0]['tested'];
		return $this->_tested;
	}
	

	public function SetUserTested()
	{
		$user_id = (int)$this->_auth->_userID;
		$query = $this->_db->Query("UPDATE exam_users SET tested=1 WHERE id='$user_id'");
		return $query;
	}
	
	
	
	public function SetUserAnswer()
	{
		$question_id = (int)$_SESSION['list_id'][$_POST['current']-1]; ////////////!!!!!!!!!
		$answer_id = (int)$_POST['answer_id'];
		$user_id = (int)$_SESSION['user_id'];
			
		if ($_POST['answer_id'] != "")
		{
			$question_id = (int)$question_id;
			$answer_id = (int)$answer_id;
			$user_id = (int)$user_id;

			$query = $this->_db->Query("SELECT id FROM exam_user_answers WHERE user_id='$user_id' AND question_id='$question_id'");

                        var_dump($query);
			if ($query['id'] == false)
			{
            echo "INSERT";
				return $this->_db->Query("INSERT INTO exam_user_answers (id, user_id, question_id, answer_id)
						VALUES (NULL, '$user_id', '$question_id', '$answer_id')");
			}
			else
			{
            echo "UPDATE";
				$user_answer_id = $query[0]["id"];
				return $this->_db->Query("UPDATE exam_user_answers SET answer_id='$answer_id' WHERE id='$user_answer_id'");
			}
		}
	}
	
	
	public function GetAllQuestionsAndAnswers()
	{
		$user_id = (int)$_SESSION['user_id'];
		
		$query = $this->_db->Query(
				"SELECT exam_answers.question_id, exam_answers.id, exam_questions.question,
					exam_answers.answer, exam_answers.right
				FROM exam_questions, exam_answers, exam_user_answers 
				WHERE  exam_questions.id=exam_answers.question_id AND exam_user_answers.user_id='$user_id'
					AND exam_user_answers.question_id=exam_questions.id 
					AND (exam_answers.right=1 OR exam_user_answers.answer_id=exam_answers.id)
				ORDER BY exam_questions.id ASC
				");
// 		echo "<br/><br/>************************************<br/>************************************";
// 		$this->printArray($query);
		
		
		$this->numberOfRightAnswers = 0;
		$this->numberOfWrongAnswers = 0;
		$this->numberOfQuestions = 0;
		$iterator = 0;
		
		foreach ($query as $k0 => $v0)
		{
			if ($k0 == 0)
			{
				$this->allResult[0]['question_id'] = $v0['question_id'];
				$this->allResult[0]['question'] = $v0['question'];
				$this->allResult[0]['answer_right'] = $v0['answer'];
				$this->allResult[0]['answer_user'] = $v0['answer'];
				$this->allResult[0]['right'] = 1;
				$iterator++;
			}
			if ($k0 > 0)
			{
				if ($query[$k0]['question_id'] == $query[$k0 - 1]['question_id'])
				{
					if ($v0['right'] == 1)
						$this->allResult[$iterator - 1]['answer_right'] = $v0['answer'];
					else
						$this->allResult[$iterator - 1]['answer_user'] = $v0['answer'];
					
					$this->allResult[$iterator - 1]['right'] = 0;
						
					$this->numberOfWrongAnswers++;
				}
				else 
				{
					$this->allResult[$iterator]['question_id'] = $v0['question_id'];
					$this->allResult[$iterator]['question'] = $v0['question'];
					if ($v0['right'] == 1)
						$this->allResult[$iterator]['answer_right'] = $v0['answer'];
					$this->allResult[$iterator]['answer_user'] = $v0['answer'];
					$this->allResult[$iterator]['right'] = 1;
					$iterator++;
				}
			}
			
			$this->numberOfQuestions++;
				
			foreach ($v0 as $k1 => $v1)
			{

			}
		}
		
		$this->numberOfQuestions = $this->numberOfQuestions - $this->numberOfWrongAnswers;
		$this->numberOfRightAnswers = $this->numberOfQuestions - $this->numberOfWrongAnswers;
		
/* 		$this->questions = $this->_db->Query(
				"SELECT questions.id, questions.question,
					answers.id, answers.question_id, answers.answer, answers.right,
					user_answers.question_id, user_answers.answer_id 
				FROM questions, answers, user_answers 
				WHERE  questions.id=answers.question_id AND user_answers.user_id='$user_id'
					AND user_answers.question_id=questions.id 
					AND (answers.right=1 OR user_answers.answer_id=answers.id)
				");
		echo "<br/><br/>************************************<br/>************************************";
		$this->printArray($this->questions);
 */		
/* 		$this->questions = $this->_db->Query(
				"SELECT questions.id, questions.question,
					answers.id, answers.question_id, answers.answer, answers.right 
				FROM questions, answers 
				WHERE answers.right=1 AND questions.id=answers.question_id");
		echo "<br/><br/>************************************<br/>************************************";
		$this->printArray($this->questions);
 */		
/* 		$this->questions = $this->_db->Query("SELECT questions.id, questions.question FROM questions");
		echo "<br/><br/>************************************<br/>************************************";
		$this->printArray($this->questions);
		
		$this->answers = $this->_db->Query("SELECT answers.id, answers.question_id, answers.answer, answers.right FROM answers");
		echo "<br/><br/>************************************<br/>************************************";
		$this->printArray($this->answers);
		
		$this->user_answers = $this->_db->Query("SELECT user_answers.question_id, user_answers.answer_id
				FROM user_answers WHERE user_answers.user_id='$user_id' ");
		echo "<br/><br/>************************************<br/>************************************";
		$this->printArray($this->user_answers);
 */	
	}
	
	protected function printArray($query)
	{
		foreach ($query as $k0 => $v0)
		{
			if($k0 == 0)
			{
				echo "<br/> ====================";
				echo "<br/> k0 = " . $k0;
				echo "<br/> v0 = " . $v0;
				echo "<br/> ====================";
				foreach ($v0 as $k1 => $v1)
				{
					echo "<br/> k1 = " . $k1;
					echo "<br/> v1 = " . $v1 . "<br/>";
				}
			}
		}
	}
	
	
}
