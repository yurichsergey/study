<?php
class Auth
{
	public $_userID;
	public $_login;
	public $_firstname;
	public $_lastname;
	public $_tested;
	public $_SessionID;
	public $_message;
	private $_db;
	
	public function __construct($db)
	{
		session_start();
		$this->_db = $db;
	}
	
	public function Login($login, $password, $remember)
	{

		$login = $this->_db->EscapeString($login);		
		$query = $this->_db->Query("SELECT * FROM exam_users WHERE login='$login'");
//		echo var_dump($query)."<br>";
//                echo $query[0]['password'] ." QQQ ". hash('sha512', $password);
//		if($query[0]['password'] != hash('sha512', $password))
		if($query['password'] != hash('sha512', $password))
		{
			$this->_message = "Извините, сочетания таких логина с паролем <br/>в базе данных не найдено";
			return false;
		}
		
		$this->_userID = $query['id'];
		$this->_login = $query['login'];
		$this->_lastname = $query['lastname'];
		$this->_firstname = $query['firstname'];
		$this->_tested = $query['tested'];
		
		$this->_SessionID = hash('sha512', "" . $login .  time() . "Satay is a very popular delicacy in Indonesia; 
				the country's diverse ethnic groups' culinary arts (see Indonesian cuisine) have produced a wide 
				variety of satays. In Indonesia, satay can be obtained from a travelling satay vendor, from a 
				street-side tent-restaurant, in an upper-class restaurant, or during traditional celebration feasts.
				In Malaysia, satay is a popular dish—especially during celebrations—and can be found throughout the 
				country. In Southern Philippines it is known as satti." );
		
		if(!$this->_db->Query("UPDATE exam_users SET session_id='$this->_SessionID' WHERE login='$login'"))
		{
			$this->_message = "На сайте проблемы. Зайдите позже";
			return false;
		}
		
		$_SESSION['user_id'] = $this->_userID;
		$_SESSION['login'] = $this->_login;
		$_SESSION['firstname'] = $this->_firstname;
		$_SESSION['lastname'] = $this->_lastname;
		$_SESSION['session_id'] = $this->_SessionID;
		$_SESSION['tested'] = $this->_tested;
	
// 		if ($remember)
// 		{
// 			setcookie('session_id', $this->_SessionID, time() + 3600 * 24 * (7 * 4 + 2));
// 			setcookie('remember', 1, time() + 3600 * 24 * (7 * 4 + 2));
// 			$_SESSION['remember'] = 1;
// 		}
		
		return true;
	}
	
	public function Logout()
	{
// 		setcookie('current', '', time() - 1);
// 		setcookie('page_current', '', time() - 1);
// 		setcookie('session_id', '', time() - 1);
// 		setcookie('remember', '', time() - 1);
// 		unset($_COOKIE['page_current']);
// 		unset($_COOKIE['session_id']);
// 		unset($_COOKIE['remember']);
// 		unset($_COOKIE['current']);
// 		unset($_COOKIE['list_id']);
		
		unset($_SESSION['user_id']);
		unset($_SESSION['login']);
		unset($_SESSION['firstname']);
		unset($_SESSION['lastname']);
		unset($_SESSION['page_current']);
		unset($_SESSION['session_id']);
		unset($_SESSION['remember']);

		unset($_SESSION['list_id']);
		unset($_SESSION['current']);
		unset($_SESSION['next']);
		unset($_SESSION['prev']);
		
	}
	
	
	public function Verification()
	{
		if(isset($_SESSION['session_id']))
		{
			$this->_SessionID = $_SESSION['session_id'];
			$this->_userID = $_SESSION['user_id'];
			$this->_login = $_SESSION['login'];
			$this->_firstname = $_SESSION['firstname'];
			$this->_lastname = $_SESSION['lastname'];
			return true;
		}
		else if (isset($_COOKIE['session_id']))
		{
			$this->_SessionID = $this->_db->EscapeString($_COOKIE['session_id']);
			$query = $this->_db->Query("SELECT * FROM exam_users WHERE session_id='$this->_SessionID'");
		
			if ($query == false)
			{
				return false;
			}
			
			$this->_userID = $query['id'];
			$this->_login = $query['login'];
			$this->_lastname = $query['lastname'];
			$this->_firstname = $query['firstname'];
		
			$_SESSION['session_id'] = $_COOKIE['session_id'];
			$_SESSION['user_id'] = $this->_userID;
			$_SESSION['login'] = $this->_login;
			$_SESSION['firstname'] = $this->_firstname;
			$_SESSION['lastname'] = $this->_lastname;
			
			return true;
		}
		return false;
	}
	
	
	public function Registration() 
	{
		$this->SaveInSessionDataOfUsers();
		
		if(!$this->CivilityDataOfPost())
			return false;
		
		$login = $this->_db->EscapeString($_POST['login']);
		$password = hash('sha512', $_POST['password']);
		$firstname = $this->_db->EscapeString($_POST['firstname']);
		$lastname = $this->_db->EscapeString($_POST['lastname']);
		
		$this->_db->Query("INSERT INTO exam_users (id, login, password, firstname, lastname)
				VALUES (NULL, '$login', '$password', '$firstname', '$lastname')");
		return true;
	}
	
	
	private function SaveInSessionDataOfUsers() 
	{
		$_SESSION['firstname'] = $_POST['firstname'];
		$_SESSION['lastname'] = $_POST['lastname'];
		$_SESSION['login'] = $_POST['login'];
	}
	
	
	private function CivilityDataOfPost()
	{
		$str = "Введите, пожалуйста, в верном формате ";
		if(!preg_match("/^[A-Za-zА-Яа-я][-0-9A-Za-zА-Яа-я\s]{0,127}$/", $_POST['firstname']))
		{
			$this->_message = $str . "имя";
			unset($_SESSION['firstname']);
			return false;
		}
		
		if(!preg_match("/^[A-Za-zА-Яа-я]([-0-9A-Za-zА-Яа-я\s]{0,127})$/", $_POST['lastname']))
		{
			if ($_POST['lastname'] != "")
			{
				$this->_message = $str . "фамилию";
				unset($_SESSION['lastname']);
				return false;
			}
		}
		
		if(!preg_match("/^[A-Za-z][-0-9A-Za-z@_.]{0,63}$/", $_POST['login']))
		{
			$this->_message = $str . "логин";
			unset($_SESSION['login']);
			return false;
		}

		$login = $this->_db->EscapeString($_POST['login']);
		if($this->_db->Query("SELECT id FROM exam_users WHERE login='$login'"))
		{
			$this->_message = "Приносим свои извинения, такой логин уже зарегистрирован. <br/>
								Пожалуйста, выберите другой логин.<br/>";
			unset($_SESSION['login']);
			return false;
		}
		
		
		if(!preg_match("/^[-0-9A-Za-z@_.]{8,128}$/", $_POST['password']))
		{
			$this->_message = $str . "пароль";
			return false;
		}
		
		if($_POST['password'] != $_POST['password2'])
		{
			$this->_message = "Введенные пароли не совпадают";
			return false;
		}
		
		return true;
	}
	
	
/*	private function print_test()
	{
		echo "<br/>";
		echo "POST page_next = " . $_POST["page_next"] . "<br/>";
		echo "POST remember = " . $_POST["remember"] . "<br/>";
		echo "POST login = " . $_POST["login"] . "<br/>";
		echo "POST logout = " . $_POST["logout"] . "<br/>";
		echo "SESSION page_current = " . $_SESSION["page_current"] . "<br/>";
		echo "SESSION remember = " . $_SESSION["remember"] . "<br/>";
		echo "SESSION login = " . $_SESSION["login"] . "<br/>";
		echo "SESSION logout = " . $_SESSION["logout"] . "<br/>";
		echo "SESSION SID = " . $_SESSION["SID"] . "<br/>";
		echo "COOKIE page_current = " . $_COOKIE["page_current"] . "<br/>";
		echo "COOKIE remember = " . $_COOKIE["remember"] . "<br/>";
		echo "COOKIE login = " . $_COOKIE["login"] . "<br/>";
		echo "COOKIE logout = " . $_COOKIE["logout"] . "<br/>";
		echo "COOKIE SID = " . $_COOKIE["SID"] . "<br/>";
//		echo " = " . $_[""] . "<br/>";
//		exit;
	}  */
	
}
