var qualificationDistance = 200;
var attempts = [120, 150, 160, 201, 203, 180, 202];
// var attempts = [120, 150, 160, 201,]; // t2
// var attempts = [120, 150, 160]; // t1
var qualified = false;
var averageBest = 0;

var sortedAttempts = attempts.slice();
// sort algorythm
for (var currentIndex = 1; currentIndex < attempts.length; currentIndex++) {
  // console.log('sortedAttempts: '+ sortedAttempts);
  if (sortedAttempts[currentIndex - 1] < sortedAttempts[currentIndex]) {
    // find previous max element
    var swapInd = 0;
    for (var i = currentIndex - 1; i >= 0; i--) {
      // console.log(sortedAttempts[currentIndex] + ' < ' + sortedAttempts[i] + ' ' + i + ' ' + swapInd + ' ' + currentIndex);
      if (sortedAttempts[currentIndex] < sortedAttempts[i]) {
        swapInd = i + 1;
        break;
      }
    }
    // console.log('main: ' + sortedAttempts[currentIndex - 1] + ' < ' + sortedAttempts[currentIndex] + ' ' + currentIndex + ', swapInd: ' + swapInd);
    
    if (swapInd !== currentIndex) {
      var localMax = sortedAttempts[currentIndex];
      for (var j = currentIndex; j > swapInd; j--) {
        // console.log(sortedAttempts[currentIndex] + ' > ' + sortedAttempts[i] + ' ' + i);
        sortedAttempts[j]=sortedAttempts[j - 1];
      }
      sortedAttempts[j] = localMax;
    }
  }
}

var bestAttempts = [];
for (var copyInd =0; copyInd < 3; copyInd++) {
  if (sortedAttempts[copyInd] !== undefined) {
    bestAttempts[copyInd] = sortedAttempts[copyInd];
    averageBest += sortedAttempts[copyInd];
  }
}
averageBest /= 3;
qualified = averageBest > qualificationDistance;

console.log('attempts: '+ attempts);
console.log('sortedAttempts: '+ sortedAttempts);
console.log('bestAttempts: '+ bestAttempts);
console.log('qualificationDistance: '+ qualificationDistance);
console.log('averageBest: '+ averageBest);
console.log('qualified: '+ qualified);
/* Техническое задание

Мяу! Я провожу тренировки и хочу понять, пройду ли квалификацию.

В течение тренировки я делаю несколько прыжков, и собираю длины прыжков в массив attempts.

Программа должна выбрать три лучших прыжка, а затем посчитать среднее значение этих трёх прыжков и записать его в переменную averageBest.

Квалификационное значение хранится в переменной qualificationDistance.

Если среднее от лучших трёх прыжков больше квалификационного значения, то я прошёл квалификацию и переменная qualified должна содержать true. Если квалификация не пройдена, то в qualified должно быть false.

*/
